(function(e, a) { for(var i in a) e[i] = a[i]; }(exports, /******/ (function(modules) { // webpackBootstrap
/******/ 	// eslint-disable-next-line no-unused-vars
/******/ 	function hotDownloadUpdateChunk(chunkId) {
/******/ 		var chunk = require("./" + "" + chunkId + "." + hotCurrentHash + ".hot-update.js");
/******/ 		hotAddUpdateChunk(chunk.id, chunk.modules);
/******/ 	}
/******/
/******/ 	// eslint-disable-next-line no-unused-vars
/******/ 	function hotDownloadManifest() {
/******/ 		try {
/******/ 			var update = require("./" + "" + hotCurrentHash + ".hot-update.json");
/******/ 		} catch (e) {
/******/ 			return Promise.resolve();
/******/ 		}
/******/ 		return Promise.resolve(update);
/******/ 	}
/******/
/******/ 	//eslint-disable-next-line no-unused-vars
/******/ 	function hotDisposeChunk(chunkId) {
/******/ 		delete installedChunks[chunkId];
/******/ 	}
/******/
/******/ 	var hotApplyOnUpdate = true;
/******/ 	var hotCurrentHash = "b6d8224258b0b080b49c"; // eslint-disable-line no-unused-vars
/******/ 	var hotRequestTimeout = 10000;
/******/ 	var hotCurrentModuleData = {};
/******/ 	var hotCurrentChildModule; // eslint-disable-line no-unused-vars
/******/ 	var hotCurrentParents = []; // eslint-disable-line no-unused-vars
/******/ 	var hotCurrentParentsTemp = []; // eslint-disable-line no-unused-vars
/******/
/******/ 	// eslint-disable-next-line no-unused-vars
/******/ 	function hotCreateRequire(moduleId) {
/******/ 		var me = installedModules[moduleId];
/******/ 		if (!me) return __webpack_require__;
/******/ 		var fn = function(request) {
/******/ 			if (me.hot.active) {
/******/ 				if (installedModules[request]) {
/******/ 					if (installedModules[request].parents.indexOf(moduleId) === -1) {
/******/ 						installedModules[request].parents.push(moduleId);
/******/ 					}
/******/ 				} else {
/******/ 					hotCurrentParents = [moduleId];
/******/ 					hotCurrentChildModule = request;
/******/ 				}
/******/ 				if (me.children.indexOf(request) === -1) {
/******/ 					me.children.push(request);
/******/ 				}
/******/ 			} else {
/******/ 				console.warn(
/******/ 					"[HMR] unexpected require(" +
/******/ 						request +
/******/ 						") from disposed module " +
/******/ 						moduleId
/******/ 				);
/******/ 				hotCurrentParents = [];
/******/ 			}
/******/ 			return __webpack_require__(request);
/******/ 		};
/******/ 		var ObjectFactory = function ObjectFactory(name) {
/******/ 			return {
/******/ 				configurable: true,
/******/ 				enumerable: true,
/******/ 				get: function() {
/******/ 					return __webpack_require__[name];
/******/ 				},
/******/ 				set: function(value) {
/******/ 					__webpack_require__[name] = value;
/******/ 				}
/******/ 			};
/******/ 		};
/******/ 		for (var name in __webpack_require__) {
/******/ 			if (
/******/ 				Object.prototype.hasOwnProperty.call(__webpack_require__, name) &&
/******/ 				name !== "e"
/******/ 			) {
/******/ 				Object.defineProperty(fn, name, ObjectFactory(name));
/******/ 			}
/******/ 		}
/******/ 		fn.e = function(chunkId) {
/******/ 			if (hotStatus === "ready") hotSetStatus("prepare");
/******/ 			hotChunksLoading++;
/******/ 			return __webpack_require__.e(chunkId).then(finishChunkLoading, function(err) {
/******/ 				finishChunkLoading();
/******/ 				throw err;
/******/ 			});
/******/
/******/ 			function finishChunkLoading() {
/******/ 				hotChunksLoading--;
/******/ 				if (hotStatus === "prepare") {
/******/ 					if (!hotWaitingFilesMap[chunkId]) {
/******/ 						hotEnsureUpdateChunk(chunkId);
/******/ 					}
/******/ 					if (hotChunksLoading === 0 && hotWaitingFiles === 0) {
/******/ 						hotUpdateDownloaded();
/******/ 					}
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 		return fn;
/******/ 	}
/******/
/******/ 	// eslint-disable-next-line no-unused-vars
/******/ 	function hotCreateModule(moduleId) {
/******/ 		var hot = {
/******/ 			// private stuff
/******/ 			_acceptedDependencies: {},
/******/ 			_declinedDependencies: {},
/******/ 			_selfAccepted: false,
/******/ 			_selfDeclined: false,
/******/ 			_disposeHandlers: [],
/******/ 			_main: hotCurrentChildModule !== moduleId,
/******/
/******/ 			// Module API
/******/ 			active: true,
/******/ 			accept: function(dep, callback) {
/******/ 				if (typeof dep === "undefined") hot._selfAccepted = true;
/******/ 				else if (typeof dep === "function") hot._selfAccepted = dep;
/******/ 				else if (typeof dep === "object")
/******/ 					for (var i = 0; i < dep.length; i++)
/******/ 						hot._acceptedDependencies[dep[i]] = callback || function() {};
/******/ 				else hot._acceptedDependencies[dep] = callback || function() {};
/******/ 			},
/******/ 			decline: function(dep) {
/******/ 				if (typeof dep === "undefined") hot._selfDeclined = true;
/******/ 				else if (typeof dep === "object")
/******/ 					for (var i = 0; i < dep.length; i++)
/******/ 						hot._declinedDependencies[dep[i]] = true;
/******/ 				else hot._declinedDependencies[dep] = true;
/******/ 			},
/******/ 			dispose: function(callback) {
/******/ 				hot._disposeHandlers.push(callback);
/******/ 			},
/******/ 			addDisposeHandler: function(callback) {
/******/ 				hot._disposeHandlers.push(callback);
/******/ 			},
/******/ 			removeDisposeHandler: function(callback) {
/******/ 				var idx = hot._disposeHandlers.indexOf(callback);
/******/ 				if (idx >= 0) hot._disposeHandlers.splice(idx, 1);
/******/ 			},
/******/
/******/ 			// Management API
/******/ 			check: hotCheck,
/******/ 			apply: hotApply,
/******/ 			status: function(l) {
/******/ 				if (!l) return hotStatus;
/******/ 				hotStatusHandlers.push(l);
/******/ 			},
/******/ 			addStatusHandler: function(l) {
/******/ 				hotStatusHandlers.push(l);
/******/ 			},
/******/ 			removeStatusHandler: function(l) {
/******/ 				var idx = hotStatusHandlers.indexOf(l);
/******/ 				if (idx >= 0) hotStatusHandlers.splice(idx, 1);
/******/ 			},
/******/
/******/ 			//inherit from previous dispose call
/******/ 			data: hotCurrentModuleData[moduleId]
/******/ 		};
/******/ 		hotCurrentChildModule = undefined;
/******/ 		return hot;
/******/ 	}
/******/
/******/ 	var hotStatusHandlers = [];
/******/ 	var hotStatus = "idle";
/******/
/******/ 	function hotSetStatus(newStatus) {
/******/ 		hotStatus = newStatus;
/******/ 		for (var i = 0; i < hotStatusHandlers.length; i++)
/******/ 			hotStatusHandlers[i].call(null, newStatus);
/******/ 	}
/******/
/******/ 	// while downloading
/******/ 	var hotWaitingFiles = 0;
/******/ 	var hotChunksLoading = 0;
/******/ 	var hotWaitingFilesMap = {};
/******/ 	var hotRequestedFilesMap = {};
/******/ 	var hotAvailableFilesMap = {};
/******/ 	var hotDeferred;
/******/
/******/ 	// The update info
/******/ 	var hotUpdate, hotUpdateNewHash;
/******/
/******/ 	function toModuleId(id) {
/******/ 		var isNumber = +id + "" === id;
/******/ 		return isNumber ? +id : id;
/******/ 	}
/******/
/******/ 	function hotCheck(apply) {
/******/ 		if (hotStatus !== "idle") {
/******/ 			throw new Error("check() is only allowed in idle status");
/******/ 		}
/******/ 		hotApplyOnUpdate = apply;
/******/ 		hotSetStatus("check");
/******/ 		return hotDownloadManifest(hotRequestTimeout).then(function(update) {
/******/ 			if (!update) {
/******/ 				hotSetStatus("idle");
/******/ 				return null;
/******/ 			}
/******/ 			hotRequestedFilesMap = {};
/******/ 			hotWaitingFilesMap = {};
/******/ 			hotAvailableFilesMap = update.c;
/******/ 			hotUpdateNewHash = update.h;
/******/
/******/ 			hotSetStatus("prepare");
/******/ 			var promise = new Promise(function(resolve, reject) {
/******/ 				hotDeferred = {
/******/ 					resolve: resolve,
/******/ 					reject: reject
/******/ 				};
/******/ 			});
/******/ 			hotUpdate = {};
/******/ 			var chunkId = "main";
/******/ 			{
/******/ 				// eslint-disable-line no-lone-blocks
/******/ 				/*globals chunkId */
/******/ 				hotEnsureUpdateChunk(chunkId);
/******/ 			}
/******/ 			if (
/******/ 				hotStatus === "prepare" &&
/******/ 				hotChunksLoading === 0 &&
/******/ 				hotWaitingFiles === 0
/******/ 			) {
/******/ 				hotUpdateDownloaded();
/******/ 			}
/******/ 			return promise;
/******/ 		});
/******/ 	}
/******/
/******/ 	// eslint-disable-next-line no-unused-vars
/******/ 	function hotAddUpdateChunk(chunkId, moreModules) {
/******/ 		if (!hotAvailableFilesMap[chunkId] || !hotRequestedFilesMap[chunkId])
/******/ 			return;
/******/ 		hotRequestedFilesMap[chunkId] = false;
/******/ 		for (var moduleId in moreModules) {
/******/ 			if (Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				hotUpdate[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if (--hotWaitingFiles === 0 && hotChunksLoading === 0) {
/******/ 			hotUpdateDownloaded();
/******/ 		}
/******/ 	}
/******/
/******/ 	function hotEnsureUpdateChunk(chunkId) {
/******/ 		if (!hotAvailableFilesMap[chunkId]) {
/******/ 			hotWaitingFilesMap[chunkId] = true;
/******/ 		} else {
/******/ 			hotRequestedFilesMap[chunkId] = true;
/******/ 			hotWaitingFiles++;
/******/ 			hotDownloadUpdateChunk(chunkId);
/******/ 		}
/******/ 	}
/******/
/******/ 	function hotUpdateDownloaded() {
/******/ 		hotSetStatus("ready");
/******/ 		var deferred = hotDeferred;
/******/ 		hotDeferred = null;
/******/ 		if (!deferred) return;
/******/ 		if (hotApplyOnUpdate) {
/******/ 			// Wrap deferred object in Promise to mark it as a well-handled Promise to
/******/ 			// avoid triggering uncaught exception warning in Chrome.
/******/ 			// See https://bugs.chromium.org/p/chromium/issues/detail?id=465666
/******/ 			Promise.resolve()
/******/ 				.then(function() {
/******/ 					return hotApply(hotApplyOnUpdate);
/******/ 				})
/******/ 				.then(
/******/ 					function(result) {
/******/ 						deferred.resolve(result);
/******/ 					},
/******/ 					function(err) {
/******/ 						deferred.reject(err);
/******/ 					}
/******/ 				);
/******/ 		} else {
/******/ 			var outdatedModules = [];
/******/ 			for (var id in hotUpdate) {
/******/ 				if (Object.prototype.hasOwnProperty.call(hotUpdate, id)) {
/******/ 					outdatedModules.push(toModuleId(id));
/******/ 				}
/******/ 			}
/******/ 			deferred.resolve(outdatedModules);
/******/ 		}
/******/ 	}
/******/
/******/ 	function hotApply(options) {
/******/ 		if (hotStatus !== "ready")
/******/ 			throw new Error("apply() is only allowed in ready status");
/******/ 		options = options || {};
/******/
/******/ 		var cb;
/******/ 		var i;
/******/ 		var j;
/******/ 		var module;
/******/ 		var moduleId;
/******/
/******/ 		function getAffectedStuff(updateModuleId) {
/******/ 			var outdatedModules = [updateModuleId];
/******/ 			var outdatedDependencies = {};
/******/
/******/ 			var queue = outdatedModules.slice().map(function(id) {
/******/ 				return {
/******/ 					chain: [id],
/******/ 					id: id
/******/ 				};
/******/ 			});
/******/ 			while (queue.length > 0) {
/******/ 				var queueItem = queue.pop();
/******/ 				var moduleId = queueItem.id;
/******/ 				var chain = queueItem.chain;
/******/ 				module = installedModules[moduleId];
/******/ 				if (!module || module.hot._selfAccepted) continue;
/******/ 				if (module.hot._selfDeclined) {
/******/ 					return {
/******/ 						type: "self-declined",
/******/ 						chain: chain,
/******/ 						moduleId: moduleId
/******/ 					};
/******/ 				}
/******/ 				if (module.hot._main) {
/******/ 					return {
/******/ 						type: "unaccepted",
/******/ 						chain: chain,
/******/ 						moduleId: moduleId
/******/ 					};
/******/ 				}
/******/ 				for (var i = 0; i < module.parents.length; i++) {
/******/ 					var parentId = module.parents[i];
/******/ 					var parent = installedModules[parentId];
/******/ 					if (!parent) continue;
/******/ 					if (parent.hot._declinedDependencies[moduleId]) {
/******/ 						return {
/******/ 							type: "declined",
/******/ 							chain: chain.concat([parentId]),
/******/ 							moduleId: moduleId,
/******/ 							parentId: parentId
/******/ 						};
/******/ 					}
/******/ 					if (outdatedModules.indexOf(parentId) !== -1) continue;
/******/ 					if (parent.hot._acceptedDependencies[moduleId]) {
/******/ 						if (!outdatedDependencies[parentId])
/******/ 							outdatedDependencies[parentId] = [];
/******/ 						addAllToSet(outdatedDependencies[parentId], [moduleId]);
/******/ 						continue;
/******/ 					}
/******/ 					delete outdatedDependencies[parentId];
/******/ 					outdatedModules.push(parentId);
/******/ 					queue.push({
/******/ 						chain: chain.concat([parentId]),
/******/ 						id: parentId
/******/ 					});
/******/ 				}
/******/ 			}
/******/
/******/ 			return {
/******/ 				type: "accepted",
/******/ 				moduleId: updateModuleId,
/******/ 				outdatedModules: outdatedModules,
/******/ 				outdatedDependencies: outdatedDependencies
/******/ 			};
/******/ 		}
/******/
/******/ 		function addAllToSet(a, b) {
/******/ 			for (var i = 0; i < b.length; i++) {
/******/ 				var item = b[i];
/******/ 				if (a.indexOf(item) === -1) a.push(item);
/******/ 			}
/******/ 		}
/******/
/******/ 		// at begin all updates modules are outdated
/******/ 		// the "outdated" status can propagate to parents if they don't accept the children
/******/ 		var outdatedDependencies = {};
/******/ 		var outdatedModules = [];
/******/ 		var appliedUpdate = {};
/******/
/******/ 		var warnUnexpectedRequire = function warnUnexpectedRequire() {
/******/ 			console.warn(
/******/ 				"[HMR] unexpected require(" + result.moduleId + ") to disposed module"
/******/ 			);
/******/ 		};
/******/
/******/ 		for (var id in hotUpdate) {
/******/ 			if (Object.prototype.hasOwnProperty.call(hotUpdate, id)) {
/******/ 				moduleId = toModuleId(id);
/******/ 				/** @type {TODO} */
/******/ 				var result;
/******/ 				if (hotUpdate[id]) {
/******/ 					result = getAffectedStuff(moduleId);
/******/ 				} else {
/******/ 					result = {
/******/ 						type: "disposed",
/******/ 						moduleId: id
/******/ 					};
/******/ 				}
/******/ 				/** @type {Error|false} */
/******/ 				var abortError = false;
/******/ 				var doApply = false;
/******/ 				var doDispose = false;
/******/ 				var chainInfo = "";
/******/ 				if (result.chain) {
/******/ 					chainInfo = "\nUpdate propagation: " + result.chain.join(" -> ");
/******/ 				}
/******/ 				switch (result.type) {
/******/ 					case "self-declined":
/******/ 						if (options.onDeclined) options.onDeclined(result);
/******/ 						if (!options.ignoreDeclined)
/******/ 							abortError = new Error(
/******/ 								"Aborted because of self decline: " +
/******/ 									result.moduleId +
/******/ 									chainInfo
/******/ 							);
/******/ 						break;
/******/ 					case "declined":
/******/ 						if (options.onDeclined) options.onDeclined(result);
/******/ 						if (!options.ignoreDeclined)
/******/ 							abortError = new Error(
/******/ 								"Aborted because of declined dependency: " +
/******/ 									result.moduleId +
/******/ 									" in " +
/******/ 									result.parentId +
/******/ 									chainInfo
/******/ 							);
/******/ 						break;
/******/ 					case "unaccepted":
/******/ 						if (options.onUnaccepted) options.onUnaccepted(result);
/******/ 						if (!options.ignoreUnaccepted)
/******/ 							abortError = new Error(
/******/ 								"Aborted because " + moduleId + " is not accepted" + chainInfo
/******/ 							);
/******/ 						break;
/******/ 					case "accepted":
/******/ 						if (options.onAccepted) options.onAccepted(result);
/******/ 						doApply = true;
/******/ 						break;
/******/ 					case "disposed":
/******/ 						if (options.onDisposed) options.onDisposed(result);
/******/ 						doDispose = true;
/******/ 						break;
/******/ 					default:
/******/ 						throw new Error("Unexception type " + result.type);
/******/ 				}
/******/ 				if (abortError) {
/******/ 					hotSetStatus("abort");
/******/ 					return Promise.reject(abortError);
/******/ 				}
/******/ 				if (doApply) {
/******/ 					appliedUpdate[moduleId] = hotUpdate[moduleId];
/******/ 					addAllToSet(outdatedModules, result.outdatedModules);
/******/ 					for (moduleId in result.outdatedDependencies) {
/******/ 						if (
/******/ 							Object.prototype.hasOwnProperty.call(
/******/ 								result.outdatedDependencies,
/******/ 								moduleId
/******/ 							)
/******/ 						) {
/******/ 							if (!outdatedDependencies[moduleId])
/******/ 								outdatedDependencies[moduleId] = [];
/******/ 							addAllToSet(
/******/ 								outdatedDependencies[moduleId],
/******/ 								result.outdatedDependencies[moduleId]
/******/ 							);
/******/ 						}
/******/ 					}
/******/ 				}
/******/ 				if (doDispose) {
/******/ 					addAllToSet(outdatedModules, [result.moduleId]);
/******/ 					appliedUpdate[moduleId] = warnUnexpectedRequire;
/******/ 				}
/******/ 			}
/******/ 		}
/******/
/******/ 		// Store self accepted outdated modules to require them later by the module system
/******/ 		var outdatedSelfAcceptedModules = [];
/******/ 		for (i = 0; i < outdatedModules.length; i++) {
/******/ 			moduleId = outdatedModules[i];
/******/ 			if (
/******/ 				installedModules[moduleId] &&
/******/ 				installedModules[moduleId].hot._selfAccepted
/******/ 			)
/******/ 				outdatedSelfAcceptedModules.push({
/******/ 					module: moduleId,
/******/ 					errorHandler: installedModules[moduleId].hot._selfAccepted
/******/ 				});
/******/ 		}
/******/
/******/ 		// Now in "dispose" phase
/******/ 		hotSetStatus("dispose");
/******/ 		Object.keys(hotAvailableFilesMap).forEach(function(chunkId) {
/******/ 			if (hotAvailableFilesMap[chunkId] === false) {
/******/ 				hotDisposeChunk(chunkId);
/******/ 			}
/******/ 		});
/******/
/******/ 		var idx;
/******/ 		var queue = outdatedModules.slice();
/******/ 		while (queue.length > 0) {
/******/ 			moduleId = queue.pop();
/******/ 			module = installedModules[moduleId];
/******/ 			if (!module) continue;
/******/
/******/ 			var data = {};
/******/
/******/ 			// Call dispose handlers
/******/ 			var disposeHandlers = module.hot._disposeHandlers;
/******/ 			for (j = 0; j < disposeHandlers.length; j++) {
/******/ 				cb = disposeHandlers[j];
/******/ 				cb(data);
/******/ 			}
/******/ 			hotCurrentModuleData[moduleId] = data;
/******/
/******/ 			// disable module (this disables requires from this module)
/******/ 			module.hot.active = false;
/******/
/******/ 			// remove module from cache
/******/ 			delete installedModules[moduleId];
/******/
/******/ 			// when disposing there is no need to call dispose handler
/******/ 			delete outdatedDependencies[moduleId];
/******/
/******/ 			// remove "parents" references from all children
/******/ 			for (j = 0; j < module.children.length; j++) {
/******/ 				var child = installedModules[module.children[j]];
/******/ 				if (!child) continue;
/******/ 				idx = child.parents.indexOf(moduleId);
/******/ 				if (idx >= 0) {
/******/ 					child.parents.splice(idx, 1);
/******/ 				}
/******/ 			}
/******/ 		}
/******/
/******/ 		// remove outdated dependency from module children
/******/ 		var dependency;
/******/ 		var moduleOutdatedDependencies;
/******/ 		for (moduleId in outdatedDependencies) {
/******/ 			if (
/******/ 				Object.prototype.hasOwnProperty.call(outdatedDependencies, moduleId)
/******/ 			) {
/******/ 				module = installedModules[moduleId];
/******/ 				if (module) {
/******/ 					moduleOutdatedDependencies = outdatedDependencies[moduleId];
/******/ 					for (j = 0; j < moduleOutdatedDependencies.length; j++) {
/******/ 						dependency = moduleOutdatedDependencies[j];
/******/ 						idx = module.children.indexOf(dependency);
/******/ 						if (idx >= 0) module.children.splice(idx, 1);
/******/ 					}
/******/ 				}
/******/ 			}
/******/ 		}
/******/
/******/ 		// Not in "apply" phase
/******/ 		hotSetStatus("apply");
/******/
/******/ 		hotCurrentHash = hotUpdateNewHash;
/******/
/******/ 		// insert new code
/******/ 		for (moduleId in appliedUpdate) {
/******/ 			if (Object.prototype.hasOwnProperty.call(appliedUpdate, moduleId)) {
/******/ 				modules[moduleId] = appliedUpdate[moduleId];
/******/ 			}
/******/ 		}
/******/
/******/ 		// call accept handlers
/******/ 		var error = null;
/******/ 		for (moduleId in outdatedDependencies) {
/******/ 			if (
/******/ 				Object.prototype.hasOwnProperty.call(outdatedDependencies, moduleId)
/******/ 			) {
/******/ 				module = installedModules[moduleId];
/******/ 				if (module) {
/******/ 					moduleOutdatedDependencies = outdatedDependencies[moduleId];
/******/ 					var callbacks = [];
/******/ 					for (i = 0; i < moduleOutdatedDependencies.length; i++) {
/******/ 						dependency = moduleOutdatedDependencies[i];
/******/ 						cb = module.hot._acceptedDependencies[dependency];
/******/ 						if (cb) {
/******/ 							if (callbacks.indexOf(cb) !== -1) continue;
/******/ 							callbacks.push(cb);
/******/ 						}
/******/ 					}
/******/ 					for (i = 0; i < callbacks.length; i++) {
/******/ 						cb = callbacks[i];
/******/ 						try {
/******/ 							cb(moduleOutdatedDependencies);
/******/ 						} catch (err) {
/******/ 							if (options.onErrored) {
/******/ 								options.onErrored({
/******/ 									type: "accept-errored",
/******/ 									moduleId: moduleId,
/******/ 									dependencyId: moduleOutdatedDependencies[i],
/******/ 									error: err
/******/ 								});
/******/ 							}
/******/ 							if (!options.ignoreErrored) {
/******/ 								if (!error) error = err;
/******/ 							}
/******/ 						}
/******/ 					}
/******/ 				}
/******/ 			}
/******/ 		}
/******/
/******/ 		// Load self accepted modules
/******/ 		for (i = 0; i < outdatedSelfAcceptedModules.length; i++) {
/******/ 			var item = outdatedSelfAcceptedModules[i];
/******/ 			moduleId = item.module;
/******/ 			hotCurrentParents = [moduleId];
/******/ 			try {
/******/ 				__webpack_require__(moduleId);
/******/ 			} catch (err) {
/******/ 				if (typeof item.errorHandler === "function") {
/******/ 					try {
/******/ 						item.errorHandler(err);
/******/ 					} catch (err2) {
/******/ 						if (options.onErrored) {
/******/ 							options.onErrored({
/******/ 								type: "self-accept-error-handler-errored",
/******/ 								moduleId: moduleId,
/******/ 								error: err2,
/******/ 								originalError: err
/******/ 							});
/******/ 						}
/******/ 						if (!options.ignoreErrored) {
/******/ 							if (!error) error = err2;
/******/ 						}
/******/ 						if (!error) error = err;
/******/ 					}
/******/ 				} else {
/******/ 					if (options.onErrored) {
/******/ 						options.onErrored({
/******/ 							type: "self-accept-errored",
/******/ 							moduleId: moduleId,
/******/ 							error: err
/******/ 						});
/******/ 					}
/******/ 					if (!options.ignoreErrored) {
/******/ 						if (!error) error = err;
/******/ 					}
/******/ 				}
/******/ 			}
/******/ 		}
/******/
/******/ 		// handle errors in accept handlers and self accepted module load
/******/ 		if (error) {
/******/ 			hotSetStatus("fail");
/******/ 			return Promise.reject(error);
/******/ 		}
/******/
/******/ 		hotSetStatus("idle");
/******/ 		return new Promise(function(resolve) {
/******/ 			resolve(outdatedModules);
/******/ 		});
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {},
/******/ 			hot: hotCreateModule(moduleId),
/******/ 			parents: (hotCurrentParentsTemp = hotCurrentParents, hotCurrentParents = [], hotCurrentParentsTemp),
/******/ 			children: []
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, hotCreateRequire(moduleId));
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// __webpack_hash__
/******/ 	__webpack_require__.h = function() { return hotCurrentHash; };
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return hotCreateRequire(0)(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/webpack/hot/log-apply-result.js":
/*!*****************************************!*\
  !*** (webpack)/hot/log-apply-result.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("/*\n\tMIT License http://www.opensource.org/licenses/mit-license.php\n\tAuthor Tobias Koppers @sokra\n*/\nmodule.exports = function(updatedModules, renewedModules) {\n\tvar unacceptedModules = updatedModules.filter(function(moduleId) {\n\t\treturn renewedModules && renewedModules.indexOf(moduleId) < 0;\n\t});\n\tvar log = __webpack_require__(/*! ./log */ \"./node_modules/webpack/hot/log.js\");\n\n\tif (unacceptedModules.length > 0) {\n\t\tlog(\n\t\t\t\"warning\",\n\t\t\t\"[HMR] The following modules couldn't be hot updated: (They would need a full reload!)\"\n\t\t);\n\t\tunacceptedModules.forEach(function(moduleId) {\n\t\t\tlog(\"warning\", \"[HMR]  - \" + moduleId);\n\t\t});\n\t}\n\n\tif (!renewedModules || renewedModules.length === 0) {\n\t\tlog(\"info\", \"[HMR] Nothing hot updated.\");\n\t} else {\n\t\tlog(\"info\", \"[HMR] Updated modules:\");\n\t\trenewedModules.forEach(function(moduleId) {\n\t\t\tif (typeof moduleId === \"string\" && moduleId.indexOf(\"!\") !== -1) {\n\t\t\t\tvar parts = moduleId.split(\"!\");\n\t\t\t\tlog.groupCollapsed(\"info\", \"[HMR]  - \" + parts.pop());\n\t\t\t\tlog(\"info\", \"[HMR]  - \" + moduleId);\n\t\t\t\tlog.groupEnd(\"info\");\n\t\t\t} else {\n\t\t\t\tlog(\"info\", \"[HMR]  - \" + moduleId);\n\t\t\t}\n\t\t});\n\t\tvar numberIds = renewedModules.every(function(moduleId) {\n\t\t\treturn typeof moduleId === \"number\";\n\t\t});\n\t\tif (numberIds)\n\t\t\tlog(\n\t\t\t\t\"info\",\n\t\t\t\t\"[HMR] Consider using the NamedModulesPlugin for module names.\"\n\t\t\t);\n\t}\n};\n\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vKHdlYnBhY2spL2hvdC9sb2ctYXBwbHktcmVzdWx0LmpzP2U1MmUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7O0FBRUE7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoiLi9ub2RlX21vZHVsZXMvd2VicGFjay9ob3QvbG9nLWFwcGx5LXJlc3VsdC5qcy5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qXG5cdE1JVCBMaWNlbnNlIGh0dHA6Ly93d3cub3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvbWl0LWxpY2Vuc2UucGhwXG5cdEF1dGhvciBUb2JpYXMgS29wcGVycyBAc29rcmFcbiovXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uKHVwZGF0ZWRNb2R1bGVzLCByZW5ld2VkTW9kdWxlcykge1xuXHR2YXIgdW5hY2NlcHRlZE1vZHVsZXMgPSB1cGRhdGVkTW9kdWxlcy5maWx0ZXIoZnVuY3Rpb24obW9kdWxlSWQpIHtcblx0XHRyZXR1cm4gcmVuZXdlZE1vZHVsZXMgJiYgcmVuZXdlZE1vZHVsZXMuaW5kZXhPZihtb2R1bGVJZCkgPCAwO1xuXHR9KTtcblx0dmFyIGxvZyA9IHJlcXVpcmUoXCIuL2xvZ1wiKTtcblxuXHRpZiAodW5hY2NlcHRlZE1vZHVsZXMubGVuZ3RoID4gMCkge1xuXHRcdGxvZyhcblx0XHRcdFwid2FybmluZ1wiLFxuXHRcdFx0XCJbSE1SXSBUaGUgZm9sbG93aW5nIG1vZHVsZXMgY291bGRuJ3QgYmUgaG90IHVwZGF0ZWQ6IChUaGV5IHdvdWxkIG5lZWQgYSBmdWxsIHJlbG9hZCEpXCJcblx0XHQpO1xuXHRcdHVuYWNjZXB0ZWRNb2R1bGVzLmZvckVhY2goZnVuY3Rpb24obW9kdWxlSWQpIHtcblx0XHRcdGxvZyhcIndhcm5pbmdcIiwgXCJbSE1SXSAgLSBcIiArIG1vZHVsZUlkKTtcblx0XHR9KTtcblx0fVxuXG5cdGlmICghcmVuZXdlZE1vZHVsZXMgfHwgcmVuZXdlZE1vZHVsZXMubGVuZ3RoID09PSAwKSB7XG5cdFx0bG9nKFwiaW5mb1wiLCBcIltITVJdIE5vdGhpbmcgaG90IHVwZGF0ZWQuXCIpO1xuXHR9IGVsc2Uge1xuXHRcdGxvZyhcImluZm9cIiwgXCJbSE1SXSBVcGRhdGVkIG1vZHVsZXM6XCIpO1xuXHRcdHJlbmV3ZWRNb2R1bGVzLmZvckVhY2goZnVuY3Rpb24obW9kdWxlSWQpIHtcblx0XHRcdGlmICh0eXBlb2YgbW9kdWxlSWQgPT09IFwic3RyaW5nXCIgJiYgbW9kdWxlSWQuaW5kZXhPZihcIiFcIikgIT09IC0xKSB7XG5cdFx0XHRcdHZhciBwYXJ0cyA9IG1vZHVsZUlkLnNwbGl0KFwiIVwiKTtcblx0XHRcdFx0bG9nLmdyb3VwQ29sbGFwc2VkKFwiaW5mb1wiLCBcIltITVJdICAtIFwiICsgcGFydHMucG9wKCkpO1xuXHRcdFx0XHRsb2coXCJpbmZvXCIsIFwiW0hNUl0gIC0gXCIgKyBtb2R1bGVJZCk7XG5cdFx0XHRcdGxvZy5ncm91cEVuZChcImluZm9cIik7XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRsb2coXCJpbmZvXCIsIFwiW0hNUl0gIC0gXCIgKyBtb2R1bGVJZCk7XG5cdFx0XHR9XG5cdFx0fSk7XG5cdFx0dmFyIG51bWJlcklkcyA9IHJlbmV3ZWRNb2R1bGVzLmV2ZXJ5KGZ1bmN0aW9uKG1vZHVsZUlkKSB7XG5cdFx0XHRyZXR1cm4gdHlwZW9mIG1vZHVsZUlkID09PSBcIm51bWJlclwiO1xuXHRcdH0pO1xuXHRcdGlmIChudW1iZXJJZHMpXG5cdFx0XHRsb2coXG5cdFx0XHRcdFwiaW5mb1wiLFxuXHRcdFx0XHRcIltITVJdIENvbnNpZGVyIHVzaW5nIHRoZSBOYW1lZE1vZHVsZXNQbHVnaW4gZm9yIG1vZHVsZSBuYW1lcy5cIlxuXHRcdFx0KTtcblx0fVxufTtcbiJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./node_modules/webpack/hot/log-apply-result.js\n");

/***/ }),

/***/ "./node_modules/webpack/hot/log.js":
/*!****************************!*\
  !*** (webpack)/hot/log.js ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("var logLevel = \"info\";\n\nfunction dummy() {}\n\nfunction shouldLog(level) {\n\tvar shouldLog =\n\t\t(logLevel === \"info\" && level === \"info\") ||\n\t\t([\"info\", \"warning\"].indexOf(logLevel) >= 0 && level === \"warning\") ||\n\t\t([\"info\", \"warning\", \"error\"].indexOf(logLevel) >= 0 && level === \"error\");\n\treturn shouldLog;\n}\n\nfunction logGroup(logFn) {\n\treturn function(level, msg) {\n\t\tif (shouldLog(level)) {\n\t\t\tlogFn(msg);\n\t\t}\n\t};\n}\n\nmodule.exports = function(level, msg) {\n\tif (shouldLog(level)) {\n\t\tif (level === \"info\") {\n\t\t\tconsole.log(msg);\n\t\t} else if (level === \"warning\") {\n\t\t\tconsole.warn(msg);\n\t\t} else if (level === \"error\") {\n\t\t\tconsole.error(msg);\n\t\t}\n\t}\n};\n\nvar group = console.group || dummy;\nvar groupCollapsed = console.groupCollapsed || dummy;\nvar groupEnd = console.groupEnd || dummy;\n\nmodule.exports.group = logGroup(group);\n\nmodule.exports.groupCollapsed = logGroup(groupCollapsed);\n\nmodule.exports.groupEnd = logGroup(groupEnd);\n\nmodule.exports.setLogLevel = function(level) {\n\tlogLevel = level;\n};\n\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vKHdlYnBhY2spL2hvdC9sb2cuanM/MWFmZCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBIiwiZmlsZSI6Ii4vbm9kZV9tb2R1bGVzL3dlYnBhY2svaG90L2xvZy5qcy5qcyIsInNvdXJjZXNDb250ZW50IjpbInZhciBsb2dMZXZlbCA9IFwiaW5mb1wiO1xuXG5mdW5jdGlvbiBkdW1teSgpIHt9XG5cbmZ1bmN0aW9uIHNob3VsZExvZyhsZXZlbCkge1xuXHR2YXIgc2hvdWxkTG9nID1cblx0XHQobG9nTGV2ZWwgPT09IFwiaW5mb1wiICYmIGxldmVsID09PSBcImluZm9cIikgfHxcblx0XHQoW1wiaW5mb1wiLCBcIndhcm5pbmdcIl0uaW5kZXhPZihsb2dMZXZlbCkgPj0gMCAmJiBsZXZlbCA9PT0gXCJ3YXJuaW5nXCIpIHx8XG5cdFx0KFtcImluZm9cIiwgXCJ3YXJuaW5nXCIsIFwiZXJyb3JcIl0uaW5kZXhPZihsb2dMZXZlbCkgPj0gMCAmJiBsZXZlbCA9PT0gXCJlcnJvclwiKTtcblx0cmV0dXJuIHNob3VsZExvZztcbn1cblxuZnVuY3Rpb24gbG9nR3JvdXAobG9nRm4pIHtcblx0cmV0dXJuIGZ1bmN0aW9uKGxldmVsLCBtc2cpIHtcblx0XHRpZiAoc2hvdWxkTG9nKGxldmVsKSkge1xuXHRcdFx0bG9nRm4obXNnKTtcblx0XHR9XG5cdH07XG59XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24obGV2ZWwsIG1zZykge1xuXHRpZiAoc2hvdWxkTG9nKGxldmVsKSkge1xuXHRcdGlmIChsZXZlbCA9PT0gXCJpbmZvXCIpIHtcblx0XHRcdGNvbnNvbGUubG9nKG1zZyk7XG5cdFx0fSBlbHNlIGlmIChsZXZlbCA9PT0gXCJ3YXJuaW5nXCIpIHtcblx0XHRcdGNvbnNvbGUud2Fybihtc2cpO1xuXHRcdH0gZWxzZSBpZiAobGV2ZWwgPT09IFwiZXJyb3JcIikge1xuXHRcdFx0Y29uc29sZS5lcnJvcihtc2cpO1xuXHRcdH1cblx0fVxufTtcblxudmFyIGdyb3VwID0gY29uc29sZS5ncm91cCB8fCBkdW1teTtcbnZhciBncm91cENvbGxhcHNlZCA9IGNvbnNvbGUuZ3JvdXBDb2xsYXBzZWQgfHwgZHVtbXk7XG52YXIgZ3JvdXBFbmQgPSBjb25zb2xlLmdyb3VwRW5kIHx8IGR1bW15O1xuXG5tb2R1bGUuZXhwb3J0cy5ncm91cCA9IGxvZ0dyb3VwKGdyb3VwKTtcblxubW9kdWxlLmV4cG9ydHMuZ3JvdXBDb2xsYXBzZWQgPSBsb2dHcm91cChncm91cENvbGxhcHNlZCk7XG5cbm1vZHVsZS5leHBvcnRzLmdyb3VwRW5kID0gbG9nR3JvdXAoZ3JvdXBFbmQpO1xuXG5tb2R1bGUuZXhwb3J0cy5zZXRMb2dMZXZlbCA9IGZ1bmN0aW9uKGxldmVsKSB7XG5cdGxvZ0xldmVsID0gbGV2ZWw7XG59O1xuIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./node_modules/webpack/hot/log.js\n");

/***/ }),

/***/ "./node_modules/webpack/hot/poll.js?1000":
/*!**********************************!*\
  !*** (webpack)/hot/poll.js?1000 ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("/* WEBPACK VAR INJECTION */(function(__resourceQuery) {/*\n\tMIT License http://www.opensource.org/licenses/mit-license.php\n\tAuthor Tobias Koppers @sokra\n*/\n/*globals __resourceQuery */\nif (true) {\n\tvar hotPollInterval = +__resourceQuery.substr(1) || 10 * 60 * 1000;\n\tvar log = __webpack_require__(/*! ./log */ \"./node_modules/webpack/hot/log.js\");\n\n\tvar checkForUpdate = function checkForUpdate(fromUpdate) {\n\t\tif (module.hot.status() === \"idle\") {\n\t\t\tmodule.hot\n\t\t\t\t.check(true)\n\t\t\t\t.then(function(updatedModules) {\n\t\t\t\t\tif (!updatedModules) {\n\t\t\t\t\t\tif (fromUpdate) log(\"info\", \"[HMR] Update applied.\");\n\t\t\t\t\t\treturn;\n\t\t\t\t\t}\n\t\t\t\t\t__webpack_require__(/*! ./log-apply-result */ \"./node_modules/webpack/hot/log-apply-result.js\")(updatedModules, updatedModules);\n\t\t\t\t\tcheckForUpdate(true);\n\t\t\t\t})\n\t\t\t\t.catch(function(err) {\n\t\t\t\t\tvar status = module.hot.status();\n\t\t\t\t\tif ([\"abort\", \"fail\"].indexOf(status) >= 0) {\n\t\t\t\t\t\tlog(\"warning\", \"[HMR] Cannot apply update.\");\n\t\t\t\t\t\tlog(\"warning\", \"[HMR] \" + err.stack || err.message);\n\t\t\t\t\t\tlog(\"warning\", \"[HMR] You need to restart the application!\");\n\t\t\t\t\t} else {\n\t\t\t\t\t\tlog(\"warning\", \"[HMR] Update failed: \" + err.stack || err.message);\n\t\t\t\t\t}\n\t\t\t\t});\n\t\t}\n\t};\n\tsetInterval(checkForUpdate, hotPollInterval);\n} else {}\n\n/* WEBPACK VAR INJECTION */}.call(this, \"?1000\"))\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vKHdlYnBhY2spL2hvdC9wb2xsLmpzPzEwYmEiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE1BQU07QUFDTjtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLENBQUMsUUFFRCIsImZpbGUiOiIuL25vZGVfbW9kdWxlcy93ZWJwYWNrL2hvdC9wb2xsLmpzPzEwMDAuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxuXHRNSVQgTGljZW5zZSBodHRwOi8vd3d3Lm9wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL21pdC1saWNlbnNlLnBocFxuXHRBdXRob3IgVG9iaWFzIEtvcHBlcnMgQHNva3JhXG4qL1xuLypnbG9iYWxzIF9fcmVzb3VyY2VRdWVyeSAqL1xuaWYgKG1vZHVsZS5ob3QpIHtcblx0dmFyIGhvdFBvbGxJbnRlcnZhbCA9ICtfX3Jlc291cmNlUXVlcnkuc3Vic3RyKDEpIHx8IDEwICogNjAgKiAxMDAwO1xuXHR2YXIgbG9nID0gcmVxdWlyZShcIi4vbG9nXCIpO1xuXG5cdHZhciBjaGVja0ZvclVwZGF0ZSA9IGZ1bmN0aW9uIGNoZWNrRm9yVXBkYXRlKGZyb21VcGRhdGUpIHtcblx0XHRpZiAobW9kdWxlLmhvdC5zdGF0dXMoKSA9PT0gXCJpZGxlXCIpIHtcblx0XHRcdG1vZHVsZS5ob3Rcblx0XHRcdFx0LmNoZWNrKHRydWUpXG5cdFx0XHRcdC50aGVuKGZ1bmN0aW9uKHVwZGF0ZWRNb2R1bGVzKSB7XG5cdFx0XHRcdFx0aWYgKCF1cGRhdGVkTW9kdWxlcykge1xuXHRcdFx0XHRcdFx0aWYgKGZyb21VcGRhdGUpIGxvZyhcImluZm9cIiwgXCJbSE1SXSBVcGRhdGUgYXBwbGllZC5cIik7XG5cdFx0XHRcdFx0XHRyZXR1cm47XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdHJlcXVpcmUoXCIuL2xvZy1hcHBseS1yZXN1bHRcIikodXBkYXRlZE1vZHVsZXMsIHVwZGF0ZWRNb2R1bGVzKTtcblx0XHRcdFx0XHRjaGVja0ZvclVwZGF0ZSh0cnVlKTtcblx0XHRcdFx0fSlcblx0XHRcdFx0LmNhdGNoKGZ1bmN0aW9uKGVycikge1xuXHRcdFx0XHRcdHZhciBzdGF0dXMgPSBtb2R1bGUuaG90LnN0YXR1cygpO1xuXHRcdFx0XHRcdGlmIChbXCJhYm9ydFwiLCBcImZhaWxcIl0uaW5kZXhPZihzdGF0dXMpID49IDApIHtcblx0XHRcdFx0XHRcdGxvZyhcIndhcm5pbmdcIiwgXCJbSE1SXSBDYW5ub3QgYXBwbHkgdXBkYXRlLlwiKTtcblx0XHRcdFx0XHRcdGxvZyhcIndhcm5pbmdcIiwgXCJbSE1SXSBcIiArIGVyci5zdGFjayB8fCBlcnIubWVzc2FnZSk7XG5cdFx0XHRcdFx0XHRsb2coXCJ3YXJuaW5nXCIsIFwiW0hNUl0gWW91IG5lZWQgdG8gcmVzdGFydCB0aGUgYXBwbGljYXRpb24hXCIpO1xuXHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHRsb2coXCJ3YXJuaW5nXCIsIFwiW0hNUl0gVXBkYXRlIGZhaWxlZDogXCIgKyBlcnIuc3RhY2sgfHwgZXJyLm1lc3NhZ2UpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSk7XG5cdFx0fVxuXHR9O1xuXHRzZXRJbnRlcnZhbChjaGVja0ZvclVwZGF0ZSwgaG90UG9sbEludGVydmFsKTtcbn0gZWxzZSB7XG5cdHRocm93IG5ldyBFcnJvcihcIltITVJdIEhvdCBNb2R1bGUgUmVwbGFjZW1lbnQgaXMgZGlzYWJsZWQuXCIpO1xufVxuIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./node_modules/webpack/hot/poll.js?1000\n");

/***/ }),

/***/ "./src/app/config/AppConfig.ts":
/*!*************************************!*\
  !*** ./src/app/config/AppConfig.ts ***!
  \*************************************/
/*! exports provided: AppConfig */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"AppConfig\", function() { return AppConfig; });\n/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! path */ \"path\");\n/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(path__WEBPACK_IMPORTED_MODULE_0__);\n\r\n/**\r\n * read default config and merge it with CONFIG environment variable\r\n */\r\nprocess.env.NODE_CONFIG_DIR = path__WEBPACK_IMPORTED_MODULE_0__[\"join\"](path__WEBPACK_IMPORTED_MODULE_0__[\"resolve\"](__dirname), '..', 'app', 'config');\r\nvar envConfig = JSON.parse(process.env.CONFIG || '{}');\r\nvar AppConfig = Object.assign({}, __webpack_require__(/*! config */ \"config\"), envConfig);\r\n\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvYXBwL2NvbmZpZy9BcHBDb25maWcudHM/NTMzYSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQWtDO0FBR2xDOztHQUVHO0FBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLEdBQUcseUNBQVMsQ0FBQyw0Q0FBWSxDQUFDLFNBQVMsQ0FBQyxFQUFFLElBQUksRUFBRSxLQUFLLEVBQUUsUUFBUSxDQUFDLENBQUM7QUFDeEYsSUFBTSxTQUFTLEdBQWUsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsQ0FBQztBQUM5RCxJQUFNLFNBQVMsR0FBZSxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxtQkFBTyxDQUFDLHNCQUFRLENBQUMsRUFBRSxTQUFTLENBQUMsQ0FBQyIsImZpbGUiOiIuL3NyYy9hcHAvY29uZmlnL0FwcENvbmZpZy50cy5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIHBhdGggICAgICBmcm9tICdwYXRoJztcclxuaW1wb3J0IHsgSUFwcENvbmZpZyB9IGZyb20gJy4vSUFwcENvbmZpZyc7XHJcblxyXG4vKipcclxuICogcmVhZCBkZWZhdWx0IGNvbmZpZyBhbmQgbWVyZ2UgaXQgd2l0aCBDT05GSUcgZW52aXJvbm1lbnQgdmFyaWFibGVcclxuICovXHJcbnByb2Nlc3MuZW52Lk5PREVfQ09ORklHX0RJUiA9IHBhdGguam9pbihwYXRoLnJlc29sdmUoX19kaXJuYW1lKSwgJy4uJywgJ2FwcCcsICdjb25maWcnKTtcclxuY29uc3QgZW52Q29uZmlnOiBJQXBwQ29uZmlnID0gSlNPTi5wYXJzZShwcm9jZXNzLmVudi5DT05GSUcgfHwgJ3t9Jyk7XHJcbmV4cG9ydCBjb25zdCBBcHBDb25maWc6IElBcHBDb25maWcgPSBPYmplY3QuYXNzaWduKHt9LCByZXF1aXJlKCdjb25maWcnKSwgZW52Q29uZmlnKTtcclxuIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/app/config/AppConfig.ts\n");

/***/ }),

/***/ "./src/server/index.ts":
/*!*****************************!*\
  !*** ./src/server/index.ts ***!
  \*****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! http */ \"http\");\n/* harmony import */ var http__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(http__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _utils_Logger__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./utils/Logger */ \"./src/server/utils/Logger.ts\");\n/* harmony import */ var _server__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./server */ \"./src/server/server.ts\");\n\r\n\r\n\r\nvar server = http__WEBPACK_IMPORTED_MODULE_0___default.a.createServer(_server__WEBPACK_IMPORTED_MODULE_2__[\"app\"]);\r\nvar port = process.env.PORT || '3000';\r\nvar currentApp = _server__WEBPACK_IMPORTED_MODULE_2__[\"app\"];\r\nserver.listen(port, function () {\r\n    _utils_Logger__WEBPACK_IMPORTED_MODULE_1__[\"Logger\"].info(\"server started at http://localhost:\" + port);\r\n});\r\nif (true) {\r\n    module.hot.accept(/*! ./server */ \"./src/server/server.ts\", function(__WEBPACK_OUTDATED_DEPENDENCIES__) { /* harmony import */ _server__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./server */ \"./src/server/server.ts\");\n(function () {\r\n        server.removeListener('request', currentApp);\r\n        server.on('request', _server__WEBPACK_IMPORTED_MODULE_2__[\"app\"]);\r\n        currentApp = _server__WEBPACK_IMPORTED_MODULE_2__[\"app\"];\r\n    })(__WEBPACK_OUTDATED_DEPENDENCIES__); });\r\n}\r\n\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvc2VydmVyL2luZGV4LnRzPzVlZTYiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBOEI7QUFDVTtBQUNOO0FBRWxDLElBQU0sTUFBTSxHQUFHLDJDQUFJLENBQUMsWUFBWSxDQUFDLDJDQUFHLENBQUMsQ0FBQztBQUN0QyxJQUFNLElBQUksR0FBVyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksSUFBSSxNQUFNLENBQUM7QUFDaEQsSUFBSSxVQUFVLEdBQUcsMkNBQUcsQ0FBQztBQUVyQixNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRTtJQUNsQixvREFBTSxDQUFDLElBQUksQ0FBQyx3Q0FBc0MsSUFBTSxDQUFDLENBQUM7QUFDNUQsQ0FBQyxDQUFDLENBQUM7QUFFSCxJQUFJLElBQVUsRUFBRTtJQUNkLE1BQU0sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLHdDQUFVLEVBQUU7QUFBQTtRQUM1QixNQUFNLENBQUMsY0FBYyxDQUFDLFNBQVMsRUFBRSxVQUFVLENBQUMsQ0FBQztRQUM3QyxNQUFNLENBQUMsRUFBRSxDQUFDLFNBQVMsRUFBRSwyQ0FBRyxDQUFDLENBQUM7UUFDMUIsVUFBVSxHQUFHLDJDQUFHLENBQUM7SUFDbkIsQ0FBQyx3Q0FBQyxDQUFDO0NBQ0oiLCJmaWxlIjoiLi9zcmMvc2VydmVyL2luZGV4LnRzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGh0dHAgICAgICAgZnJvbSAnaHR0cCc7XHJcbmltcG9ydCB7IExvZ2dlciB9IGZyb20gJy4vdXRpbHMvTG9nZ2VyJztcclxuaW1wb3J0IHsgYXBwIH0gICAgZnJvbSAnLi9zZXJ2ZXInO1xyXG5cclxuY29uc3Qgc2VydmVyID0gaHR0cC5jcmVhdGVTZXJ2ZXIoYXBwKTtcclxuY29uc3QgcG9ydDogc3RyaW5nID0gcHJvY2Vzcy5lbnYuUE9SVCB8fCAnMzAwMCc7XHJcbmxldCBjdXJyZW50QXBwID0gYXBwO1xyXG5cclxuc2VydmVyLmxpc3Rlbihwb3J0LCAoKSA9PiB7XHJcbiAgTG9nZ2VyLmluZm8oYHNlcnZlciBzdGFydGVkIGF0IGh0dHA6Ly9sb2NhbGhvc3Q6JHtwb3J0fWApO1xyXG59KTtcclxuXHJcbmlmIChtb2R1bGUuaG90KSB7XHJcbiAgbW9kdWxlLmhvdC5hY2NlcHQoJy4vc2VydmVyJywgKCkgPT4ge1xyXG4gICAgc2VydmVyLnJlbW92ZUxpc3RlbmVyKCdyZXF1ZXN0JywgY3VycmVudEFwcCk7XHJcbiAgICBzZXJ2ZXIub24oJ3JlcXVlc3QnLCBhcHApO1xyXG4gICAgY3VycmVudEFwcCA9IGFwcDtcclxuICB9KTtcclxufVxyXG4iXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./src/server/index.ts\n");

/***/ }),

/***/ "./src/server/middlewares/index.ts":
/*!*****************************************!*\
  !*** ./src/server/middlewares/index.ts ***!
  \*****************************************/
/*! exports provided: applyMiddlewares */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"applyMiddlewares\", function() { return applyMiddlewares; });\n/* harmony import */ var body_parser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! body-parser */ \"body-parser\");\n/* harmony import */ var body_parser__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(body_parser__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var cookie_parser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! cookie-parser */ \"cookie-parser\");\n/* harmony import */ var cookie_parser__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(cookie_parser__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var compression__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! compression */ \"compression\");\n/* harmony import */ var compression__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(compression__WEBPACK_IMPORTED_MODULE_2__);\n\r\n\r\n\r\nvar applyMiddlewares = function (app) {\r\n    app.use(body_parser__WEBPACK_IMPORTED_MODULE_0__[\"json\"]());\r\n    app.use(cookie_parser__WEBPACK_IMPORTED_MODULE_1__());\r\n    app.use(compression__WEBPACK_IMPORTED_MODULE_2__({ threshold: 0 }));\r\n};\r\n\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvc2VydmVyL21pZGRsZXdhcmVzL2luZGV4LnRzP2RiMjgiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFDNEM7QUFDRTtBQUNGO0FBRXJDLElBQU0sZ0JBQWdCLEdBQUcsVUFBQyxHQUF3QjtJQUN2RCxHQUFHLENBQUMsR0FBRyxDQUFDLGdEQUFlLEVBQUUsQ0FBQyxDQUFDO0lBQzNCLEdBQUcsQ0FBQyxHQUFHLENBQUMsMENBQVksRUFBRSxDQUFDLENBQUM7SUFDeEIsR0FBRyxDQUFDLEdBQUcsQ0FBQyx3Q0FBVyxDQUFDLEVBQUUsU0FBUyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztBQUN6QyxDQUFDLENBQUMiLCJmaWxlIjoiLi9zcmMvc2VydmVyL21pZGRsZXdhcmVzL2luZGV4LnRzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgRXhwcmVzcyAgICAgIGZyb20gJ2V4cHJlc3MnO1xyXG5pbXBvcnQgKiBhcyBib2R5UGFyc2VyICAgZnJvbSAnYm9keS1wYXJzZXInO1xyXG5pbXBvcnQgKiBhcyBjb29raWVQYXJzZXIgZnJvbSAnY29va2llLXBhcnNlcic7XHJcbmltcG9ydCAqIGFzIGNvbXByZXNzaW9uICBmcm9tICdjb21wcmVzc2lvbic7XHJcblxyXG5leHBvcnQgY29uc3QgYXBwbHlNaWRkbGV3YXJlcyA9IChhcHA6IEV4cHJlc3MuQXBwbGljYXRpb24pID0+IHtcclxuICBhcHAudXNlKGJvZHlQYXJzZXIuanNvbigpKTtcclxuICBhcHAudXNlKGNvb2tpZVBhcnNlcigpKTtcclxuICBhcHAudXNlKGNvbXByZXNzaW9uKHsgdGhyZXNob2xkOiAwIH0pKTtcclxufTtcclxuIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/server/middlewares/index.ts\n");

/***/ }),

/***/ "./src/server/routes/CounterRoutes.ts":
/*!********************************************!*\
  !*** ./src/server/routes/CounterRoutes.ts ***!
  \********************************************/
/*! exports provided: CounterRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"CounterRoutes\", function() { return CounterRoutes; });\nvar CounterRoutes = function (app) {\r\n    app.put('/counter/increment', function (req, res) {\r\n        setTimeout(function () {\r\n            res\r\n                .status(200)\r\n                .json({ count: parseInt(req.body.count, 10) + 1 });\r\n        }, 200);\r\n    });\r\n    app.put('/counter/decrement', function (req, res) {\r\n        setTimeout(function () {\r\n            res\r\n                .status(200)\r\n                .json({ count: parseInt(req.body.count, 10) - 1 });\r\n        }, 200);\r\n    });\r\n};\r\n\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvc2VydmVyL3JvdXRlcy9Db3VudGVyUm91dGVzLnRzPzIzYzAiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFHTyxJQUFNLGFBQWEsR0FBRyxVQUFDLEdBQXdCO0lBQ3BELEdBQUcsQ0FBQyxHQUFHLENBQUMsb0JBQW9CLEVBQUUsVUFBQyxHQUFZLEVBQUUsR0FBYTtRQUN4RCxVQUFVLENBQUM7WUFDVCxHQUFHO2lCQUNGLE1BQU0sQ0FBQyxHQUFHLENBQUM7aUJBQ1gsSUFBSSxDQUFDLEVBQUUsS0FBSyxFQUFFLFFBQVEsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ3JELENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztJQUNWLENBQUMsQ0FBQyxDQUFDO0lBRUgsR0FBRyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsRUFBRSxVQUFDLEdBQVksRUFBRSxHQUFhO1FBQ3hELFVBQVUsQ0FBQztZQUNULEdBQUc7aUJBQ0YsTUFBTSxDQUFDLEdBQUcsQ0FBQztpQkFDWCxJQUFJLENBQUMsRUFBRSxLQUFLLEVBQUUsUUFBUSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDckQsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQ1YsQ0FBQyxDQUFDLENBQUM7QUFDTCxDQUFDLENBQUMiLCJmaWxlIjoiLi9zcmMvc2VydmVyL3JvdXRlcy9Db3VudGVyUm91dGVzLnRzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgRXhwcmVzcyAgICAgICAgICBmcm9tICdleHByZXNzJztcclxuaW1wb3J0IHsgUmVxdWVzdCwgUmVzcG9uc2UgfSBmcm9tICdleHByZXNzJztcclxuXHJcbmV4cG9ydCBjb25zdCBDb3VudGVyUm91dGVzID0gKGFwcDogRXhwcmVzcy5BcHBsaWNhdGlvbikgPT4ge1xyXG4gIGFwcC5wdXQoJy9jb3VudGVyL2luY3JlbWVudCcsIChyZXE6IFJlcXVlc3QsIHJlczogUmVzcG9uc2UpID0+IHtcclxuICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICByZXNcclxuICAgICAgLnN0YXR1cygyMDApXHJcbiAgICAgIC5qc29uKHsgY291bnQ6IHBhcnNlSW50KHJlcS5ib2R5LmNvdW50LCAxMCkgKyAxIH0pO1xyXG4gICAgfSwgMjAwKTtcclxuICB9KTtcclxuXHJcbiAgYXBwLnB1dCgnL2NvdW50ZXIvZGVjcmVtZW50JywgKHJlcTogUmVxdWVzdCwgcmVzOiBSZXNwb25zZSkgPT4ge1xyXG4gICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgIHJlc1xyXG4gICAgICAuc3RhdHVzKDIwMClcclxuICAgICAgLmpzb24oeyBjb3VudDogcGFyc2VJbnQocmVxLmJvZHkuY291bnQsIDEwKSAtIDEgfSk7XHJcbiAgICB9LCAyMDApO1xyXG4gIH0pO1xyXG59O1xyXG4iXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./src/server/routes/CounterRoutes.ts\n");

/***/ }),

/***/ "./src/server/routes/DemoRoutes.ts":
/*!*****************************************!*\
  !*** ./src/server/routes/DemoRoutes.ts ***!
  \*****************************************/
/*! exports provided: DemoRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"DemoRoutes\", function() { return DemoRoutes; });\n/* harmony import */ var _utils_Utils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../utils/Utils */ \"./src/server/utils/Utils.ts\");\n\r\nvar DemoRoutes = function (app) {\r\n    /**\r\n     * http -> https redirect for heroku\r\n     */\r\n    app.get('*', function (req, res, next) {\r\n        var host = req.headers.host || 'localhost:3000';\r\n        var redirect = \"https://\" + host + req.url;\r\n        if (req.headers['x-forwarded-proto'] && req.headers['x-forwarded-proto'] !== 'https') {\r\n            res.redirect(redirect);\r\n        }\r\n        else {\r\n            next();\r\n        }\r\n    });\r\n    app.use('/storybook', Object(_utils_Utils__WEBPACK_IMPORTED_MODULE_0__[\"serve\"])('../../storybook-static'));\r\n    app.use('/docs', Object(_utils_Utils__WEBPACK_IMPORTED_MODULE_0__[\"serve\"])('../docs'));\r\n};\r\n\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvc2VydmVyL3JvdXRlcy9EZW1vUm91dGVzLnRzP2I2N2QiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBRW1EO0FBRTVDLElBQU0sVUFBVSxHQUFHLFVBQUMsR0FBd0I7SUFDakQ7O09BRUc7SUFDSCxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxVQUFDLEdBQVksRUFBRSxHQUFhLEVBQUUsSUFBUztRQUNsRCxJQUFNLElBQUksR0FBVyxHQUFHLENBQUMsT0FBTyxDQUFDLElBQUksSUFBSSxnQkFBZ0IsQ0FBQztRQUMxRCxJQUFNLFFBQVEsR0FBVyxhQUFXLElBQU0sR0FBRyxHQUFHLENBQUMsR0FBRyxDQUFDO1FBRXJELElBQUksR0FBRyxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsS0FBSyxPQUFPLEVBQUU7WUFDcEYsR0FBRyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUN4QjthQUFNO1lBQ0wsSUFBSSxFQUFFLENBQUM7U0FDUjtJQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0gsR0FBRyxDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUUsMERBQUssQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDLENBQUM7SUFDdkQsR0FBRyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsMERBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO0FBQ3JDLENBQUMsQ0FBQyIsImZpbGUiOiIuL3NyYy9zZXJ2ZXIvcm91dGVzL0RlbW9Sb3V0ZXMudHMuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBFeHByZXNzICAgICAgICAgIGZyb20gJ2V4cHJlc3MnO1xyXG5pbXBvcnQgeyBSZXF1ZXN0LCBSZXNwb25zZSB9IGZyb20gJ2V4cHJlc3MnO1xyXG5pbXBvcnQgeyBzZXJ2ZSB9ICAgICAgICAgICAgIGZyb20gJy4uL3V0aWxzL1V0aWxzJztcclxuXHJcbmV4cG9ydCBjb25zdCBEZW1vUm91dGVzID0gKGFwcDogRXhwcmVzcy5BcHBsaWNhdGlvbikgPT4ge1xyXG4gIC8qKlxyXG4gICAqIGh0dHAgLT4gaHR0cHMgcmVkaXJlY3QgZm9yIGhlcm9rdVxyXG4gICAqL1xyXG4gIGFwcC5nZXQoJyonLCAocmVxOiBSZXF1ZXN0LCByZXM6IFJlc3BvbnNlLCBuZXh0OiBhbnkpID0+IHtcclxuICAgIGNvbnN0IGhvc3Q6IHN0cmluZyA9IHJlcS5oZWFkZXJzLmhvc3QgfHwgJ2xvY2FsaG9zdDozMDAwJztcclxuICAgIGNvbnN0IHJlZGlyZWN0OiBzdHJpbmcgPSBgaHR0cHM6Ly8ke2hvc3R9YCArIHJlcS51cmw7XHJcblxyXG4gICAgaWYgKHJlcS5oZWFkZXJzWyd4LWZvcndhcmRlZC1wcm90byddICYmIHJlcS5oZWFkZXJzWyd4LWZvcndhcmRlZC1wcm90byddICE9PSAnaHR0cHMnKSB7XHJcbiAgICAgIHJlcy5yZWRpcmVjdChyZWRpcmVjdCk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBuZXh0KCk7XHJcbiAgICB9XHJcbiAgfSk7XHJcbiAgYXBwLnVzZSgnL3N0b3J5Ym9vaycsIHNlcnZlKCcuLi8uLi9zdG9yeWJvb2stc3RhdGljJykpO1xyXG4gIGFwcC51c2UoJy9kb2NzJywgc2VydmUoJy4uL2RvY3MnKSk7XHJcbn07XHJcbiJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/server/routes/DemoRoutes.ts\n");

/***/ }),

/***/ "./src/server/routes/SSRRoutes.ts":
/*!****************************************!*\
  !*** ./src/server/routes/SSRRoutes.ts ***!
  \****************************************/
/*! exports provided: SSRRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"SSRRoutes\", function() { return SSRRoutes; });\n/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! fs */ \"fs\");\n/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(fs__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var accept_language__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! accept-language */ \"accept-language\");\n/* harmony import */ var accept_language__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(accept_language__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _utils_Logger__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../utils/Logger */ \"./src/server/utils/Logger.ts\");\n/* harmony import */ var _app_config_AppConfig__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../app/config/AppConfig */ \"./src/app/config/AppConfig.ts\");\n/* harmony import */ var _utils_Utils__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../utils/Utils */ \"./src/server/utils/Utils.ts\");\n\r\n\r\n\r\n\r\n\r\nvar renderer;\r\nvar createRenderer = function (bundle, template) {\r\n    renderer = function(module){return require(module);}('vue-server-renderer').createBundleRenderer(bundle, {\r\n        template: template,\r\n        cache: function(module){return require(module);}('lru-cache')({\r\n            max: 1000,\r\n            maxAge: 1000 * 60 * 15,\r\n        }),\r\n    });\r\n};\r\nvar setHeaders = function (res) {\r\n    res.setHeader('X-Content-Type-Options', 'nosniff');\r\n    res.setHeader('X-Frame-Options', 'DENY');\r\n    res.setHeader('X-XSS-Protection', '1; mode=block');\r\n    res.setHeader('Strict-Transport-Security', 'max-age=10886400; includeSubDomains; preload');\r\n    res.setHeader('Content-Type', 'text/html');\r\n    res.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate');\r\n    res.setHeader('Pragma', 'no-cache');\r\n    res.setHeader('Expires', '0');\r\n    res.setHeader('max-age', '0');\r\n};\r\nvar packageJson = JSON.parse(fs__WEBPACK_IMPORTED_MODULE_0__[\"readFileSync\"](Object(_utils_Utils__WEBPACK_IMPORTED_MODULE_4__[\"resolve\"])('../../package.json')).toString());\r\nvar SSRRoutes = function (app) {\r\n    if (_utils_Utils__WEBPACK_IMPORTED_MODULE_4__[\"isProd\"]) {\r\n        var bundle = function(module){return require(module);}('./vue-ssr-bundle.json');\r\n        var template = fs__WEBPACK_IMPORTED_MODULE_0__[\"readFileSync\"](Object(_utils_Utils__WEBPACK_IMPORTED_MODULE_4__[\"resolve\"])('../client/index.html'), 'utf-8');\r\n        createRenderer(bundle, template);\r\n    }\r\n    else {\r\n        var devServer = function(module){return require(module);}('./dev-server.js').default;\r\n        devServer(app, function (bundle, template) {\r\n            createRenderer(bundle, template);\r\n        });\r\n    }\r\n    app.get('*', function (req, res) {\r\n        setHeaders(res);\r\n        if (!renderer) {\r\n            return res.end('waiting for compilation... refresh in a moment.');\r\n        }\r\n        accept_language__WEBPACK_IMPORTED_MODULE_1___default.a.languages(packageJson.config['supported-locales']);\r\n        var startTime = Date.now();\r\n        var acceptLang = req.headers['accept-language']\r\n            ? req.headers['accept-language'].toString()\r\n            : packageJson.config['default-locale'];\r\n        var defaultLang = accept_language__WEBPACK_IMPORTED_MODULE_1___default.a.get(acceptLang);\r\n        var errorHandler = function (err, renderRedirect) {\r\n            if (err && err.code === 404) {\r\n                res.status(404);\r\n                _utils_Logger__WEBPACK_IMPORTED_MODULE_2__[\"Logger\"].warn('unsupported route: %s; error: %s', req.url, JSON.stringify(err, Object.getOwnPropertyNames(err)));\r\n                renderRedirect('/not-found', true);\r\n            }\r\n            else {\r\n                res.status(500);\r\n                _utils_Logger__WEBPACK_IMPORTED_MODULE_2__[\"Logger\"].error('error during rendering: %s; error: %s', req.url, JSON.stringify(err, Object.getOwnPropertyNames(err)));\r\n                renderRedirect('/error', true);\r\n            }\r\n        };\r\n        var render = function (url, redirect) {\r\n            if (redirect === void 0) { redirect = false; }\r\n            var serverContext = {\r\n                url: url,\r\n                cookies: req.cookies,\r\n                acceptLanguage: defaultLang,\r\n                htmlLang: defaultLang.substr(0, 2),\r\n                appConfig: _app_config_AppConfig__WEBPACK_IMPORTED_MODULE_3__[\"AppConfig\"],\r\n                redirect: redirect,\r\n            };\r\n            renderer\r\n                .renderToStream(serverContext)\r\n                .on('error', function (err) { return errorHandler(err, render); })\r\n                .on('end', function () { return _utils_Logger__WEBPACK_IMPORTED_MODULE_2__[\"Logger\"].debug(\"whole request: \" + (Date.now() - startTime) + \"ms\"); })\r\n                .pipe(res);\r\n        };\r\n        render(req.url);\r\n    });\r\n};\r\n\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvc2VydmVyL3JvdXRlcy9TU1JSb3V0ZXMudHM/MDJiNyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFFdUM7QUFDYTtBQUdBO0FBQ1c7QUFDWjtBQUVuRCxJQUFJLFFBQXdCLENBQUM7QUFFN0IsSUFBTSxjQUFjLEdBQUcsVUFBQyxNQUFjLEVBQUUsUUFBZ0I7SUFDdEQsUUFBUSxHQUFHLHlDQUFXLENBQUMscUJBQXFCLENBQUMsQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLEVBQUU7UUFDekUsUUFBUTtRQUNSLEtBQUssRUFBRSx5Q0FBVyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQ0UsR0FBRyxFQUFLLElBQUk7WUFDWixNQUFNLEVBQUUsSUFBSSxHQUFHLEVBQUUsR0FBRyxFQUFFO1NBQ3ZCLENBQUM7S0FDbkMsQ0FBQyxDQUFDO0FBQ0wsQ0FBQyxDQUFDO0FBQ0YsSUFBTSxVQUFVLEdBQUcsVUFBQyxHQUFhO0lBQy9CLEdBQUcsQ0FBQyxTQUFTLENBQUMsd0JBQXdCLEVBQUUsU0FBUyxDQUFDLENBQUM7SUFDbkQsR0FBRyxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsRUFBRSxNQUFNLENBQUMsQ0FBQztJQUN6QyxHQUFHLENBQUMsU0FBUyxDQUFDLGtCQUFrQixFQUFFLGVBQWUsQ0FBQyxDQUFDO0lBQ25ELEdBQUcsQ0FBQyxTQUFTLENBQUMsMkJBQTJCLEVBQUUsOENBQThDLENBQUMsQ0FBQztJQUMzRixHQUFHLENBQUMsU0FBUyxDQUFDLGNBQWMsRUFBRSxXQUFXLENBQUMsQ0FBQztJQUMzQyxHQUFHLENBQUMsU0FBUyxDQUFDLGVBQWUsRUFBRSxxQ0FBcUMsQ0FBQyxDQUFDO0lBQ3RFLEdBQUcsQ0FBQyxTQUFTLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQ3BDLEdBQUcsQ0FBQyxTQUFTLENBQUMsU0FBUyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQzlCLEdBQUcsQ0FBQyxTQUFTLENBQUMsU0FBUyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0FBQ2hDLENBQUMsQ0FBQztBQUNGLElBQU0sV0FBVyxHQUFRLElBQUksQ0FBQyxLQUFLLENBQUMsK0NBQWUsQ0FBQyw0REFBTyxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO0FBRXhGLElBQU0sU0FBUyxHQUFHLFVBQUMsR0FBd0I7SUFDaEQsSUFBSSxtREFBTSxFQUFFO1FBQ1YsSUFBTSxNQUFNLEdBQVEseUNBQVcsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDO1FBQ3pELElBQU0sUUFBUSxHQUFXLCtDQUFlLENBQUMsNERBQU8sQ0FBQyxzQkFBc0IsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBRW5GLGNBQWMsQ0FBQyxNQUFNLEVBQUUsUUFBUSxDQUFDLENBQUM7S0FDbEM7U0FBTTtRQUNMLElBQU0sU0FBUyxHQUFRLHlDQUFXLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxPQUFPLENBQUM7UUFFOUQsU0FBUyxDQUFDLEdBQUcsRUFBRSxVQUFDLE1BQWMsRUFBRSxRQUFnQjtZQUM5QyxjQUFjLENBQUMsTUFBTSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBQ25DLENBQUMsQ0FBQyxDQUFDO0tBQ0o7SUFFRCxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxVQUFDLEdBQVksRUFBRSxHQUFhO1FBQ3ZDLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUVoQixJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2IsT0FBTyxHQUFHLENBQUMsR0FBRyxDQUFDLGlEQUFpRCxDQUFDLENBQUM7U0FDbkU7UUFFRCxzREFBYyxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQztRQUVsRSxJQUFNLFNBQVMsR0FBVyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7UUFDckMsSUFBTSxVQUFVLEdBQVcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQztZQUM5QixDQUFDLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLFFBQVEsRUFBRTtZQUMzQyxDQUFDLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQ2xFLElBQU0sV0FBVyxHQUFXLHNEQUFjLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQzNELElBQU0sWUFBWSxHQUFHLFVBQUMsR0FBUSxFQUFFLGNBQW1CO1lBQ2pELElBQUksR0FBRyxJQUFJLEdBQUcsQ0FBQyxJQUFJLEtBQUssR0FBRyxFQUFFO2dCQUMzQixHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUNoQixvREFBTSxDQUFDLElBQUksQ0FBQyxrQ0FBa0MsRUFBRSxHQUFHLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQy9HLGNBQWMsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLENBQUM7YUFDcEM7aUJBQU07Z0JBQ0wsR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDaEIsb0RBQU0sQ0FBQyxLQUFLLENBQUMsdUNBQXVDLEVBQUUsR0FBRyxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsRUFBRSxNQUFNLENBQUMsbUJBQW1CLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNySCxjQUFjLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDO2FBQ2hDO1FBQ0gsQ0FBQyxDQUFDO1FBQ0YsSUFBTSxNQUFNLEdBQUcsVUFBQyxHQUFXLEVBQUUsUUFBeUI7WUFBekIsMkNBQXlCO1lBQ3BELElBQU0sYUFBYSxHQUFtQjtnQkFDcEMsR0FBRztnQkFDSCxPQUFPLEVBQVMsR0FBRyxDQUFDLE9BQU87Z0JBQzNCLGNBQWMsRUFBRSxXQUFXO2dCQUMzQixRQUFRLEVBQVEsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUN4QyxTQUFTLEVBQU8sK0RBQVM7Z0JBQ3pCLFFBQVE7YUFDVCxDQUFDO1lBRUYsUUFBUTtpQkFDUCxjQUFjLENBQUMsYUFBYSxDQUFDO2lCQUM3QixFQUFFLENBQUMsT0FBTyxFQUFFLFVBQUMsR0FBUSxJQUFLLG1CQUFZLENBQUMsR0FBRyxFQUFFLE1BQU0sQ0FBQyxFQUF6QixDQUF5QixDQUFDO2lCQUNwRCxFQUFFLENBQUMsS0FBSyxFQUFFLGNBQU0sMkRBQU0sQ0FBQyxLQUFLLENBQUMscUJBQWtCLElBQUksQ0FBQyxHQUFHLEVBQUUsR0FBRyxTQUFTLFFBQUksQ0FBQyxFQUExRCxDQUEwRCxDQUFDO2lCQUMzRSxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDYixDQUFDLENBQUM7UUFFRixNQUFNLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ2xCLENBQUMsQ0FBQyxDQUFDO0FBQ0wsQ0FBQyxDQUFDIiwiZmlsZSI6Ii4vc3JjL3NlcnZlci9yb3V0ZXMvU1NSUm91dGVzLnRzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgRXhwcmVzcyAgICAgICAgICBmcm9tICdleHByZXNzJztcclxuaW1wb3J0IHsgUmVxdWVzdCwgUmVzcG9uc2UgfSBmcm9tICdleHByZXNzJztcclxuaW1wb3J0ICogYXMgZnMgICAgICAgICAgICAgICBmcm9tICdmcyc7XHJcbmltcG9ydCBhY2NlcHRMYW5ndWFnZSAgICAgICAgZnJvbSAnYWNjZXB0LWxhbmd1YWdlJztcclxuaW1wb3J0IHsgQnVuZGxlUmVuZGVyZXIgfSAgICBmcm9tICd2dWUtc2VydmVyLXJlbmRlcmVyJztcclxuaW1wb3J0IHsgSVNlcnZlckNvbnRleHQgfSAgICBmcm9tICcuLi9pc29tb3JwaGljJztcclxuaW1wb3J0IHsgTG9nZ2VyIH0gICAgICAgICAgICBmcm9tICcuLi91dGlscy9Mb2dnZXInO1xyXG5pbXBvcnQgeyBBcHBDb25maWcgfSAgICAgICAgIGZyb20gJy4uLy4uL2FwcC9jb25maWcvQXBwQ29uZmlnJztcclxuaW1wb3J0IHsgaXNQcm9kLCByZXNvbHZlIH0gICBmcm9tICcuLi91dGlscy9VdGlscyc7XHJcblxyXG5sZXQgcmVuZGVyZXI6IEJ1bmRsZVJlbmRlcmVyO1xyXG5cclxuY29uc3QgY3JlYXRlUmVuZGVyZXIgPSAoYnVuZGxlOiBzdHJpbmcsIHRlbXBsYXRlOiBzdHJpbmcpOiB2b2lkID0+IHtcclxuICByZW5kZXJlciA9IG5vZGVSZXF1aXJlKCd2dWUtc2VydmVyLXJlbmRlcmVyJykuY3JlYXRlQnVuZGxlUmVuZGVyZXIoYnVuZGxlLCB7XHJcbiAgICB0ZW1wbGF0ZSxcclxuICAgIGNhY2hlOiBub2RlUmVxdWlyZSgnbHJ1LWNhY2hlJykoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1heDogICAgMTAwMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXhBZ2U6IDEwMDAgKiA2MCAqIDE1LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KSxcclxuICB9KTtcclxufTtcclxuY29uc3Qgc2V0SGVhZGVycyA9IChyZXM6IFJlc3BvbnNlKTogdm9pZCA9PiB7XHJcbiAgcmVzLnNldEhlYWRlcignWC1Db250ZW50LVR5cGUtT3B0aW9ucycsICdub3NuaWZmJyk7XHJcbiAgcmVzLnNldEhlYWRlcignWC1GcmFtZS1PcHRpb25zJywgJ0RFTlknKTtcclxuICByZXMuc2V0SGVhZGVyKCdYLVhTUy1Qcm90ZWN0aW9uJywgJzE7IG1vZGU9YmxvY2snKTtcclxuICByZXMuc2V0SGVhZGVyKCdTdHJpY3QtVHJhbnNwb3J0LVNlY3VyaXR5JywgJ21heC1hZ2U9MTA4ODY0MDA7IGluY2x1ZGVTdWJEb21haW5zOyBwcmVsb2FkJyk7XHJcbiAgcmVzLnNldEhlYWRlcignQ29udGVudC1UeXBlJywgJ3RleHQvaHRtbCcpO1xyXG4gIHJlcy5zZXRIZWFkZXIoJ0NhY2hlLUNvbnRyb2wnLCAnbm8tY2FjaGUsIG5vLXN0b3JlLCBtdXN0LXJldmFsaWRhdGUnKTtcclxuICByZXMuc2V0SGVhZGVyKCdQcmFnbWEnLCAnbm8tY2FjaGUnKTtcclxuICByZXMuc2V0SGVhZGVyKCdFeHBpcmVzJywgJzAnKTtcclxuICByZXMuc2V0SGVhZGVyKCdtYXgtYWdlJywgJzAnKTtcclxufTtcclxuY29uc3QgcGFja2FnZUpzb246IGFueSA9IEpTT04ucGFyc2UoZnMucmVhZEZpbGVTeW5jKHJlc29sdmUoJy4uLy4uL3BhY2thZ2UuanNvbicpKS50b1N0cmluZygpKTtcclxuXHJcbmV4cG9ydCBjb25zdCBTU1JSb3V0ZXMgPSAoYXBwOiBFeHByZXNzLkFwcGxpY2F0aW9uKTogYW55ID0+IHtcclxuICBpZiAoaXNQcm9kKSB7XHJcbiAgICBjb25zdCBidW5kbGU6IGFueSA9IG5vZGVSZXF1aXJlKCcuL3Z1ZS1zc3ItYnVuZGxlLmpzb24nKTtcclxuICAgIGNvbnN0IHRlbXBsYXRlOiBzdHJpbmcgPSBmcy5yZWFkRmlsZVN5bmMocmVzb2x2ZSgnLi4vY2xpZW50L2luZGV4Lmh0bWwnKSwgJ3V0Zi04Jyk7XHJcblxyXG4gICAgY3JlYXRlUmVuZGVyZXIoYnVuZGxlLCB0ZW1wbGF0ZSk7XHJcbiAgfSBlbHNlIHtcclxuICAgIGNvbnN0IGRldlNlcnZlcjogYW55ID0gbm9kZVJlcXVpcmUoJy4vZGV2LXNlcnZlci5qcycpLmRlZmF1bHQ7XHJcblxyXG4gICAgZGV2U2VydmVyKGFwcCwgKGJ1bmRsZTogc3RyaW5nLCB0ZW1wbGF0ZTogc3RyaW5nKSA9PiB7XHJcbiAgICAgIGNyZWF0ZVJlbmRlcmVyKGJ1bmRsZSwgdGVtcGxhdGUpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBhcHAuZ2V0KCcqJywgKHJlcTogUmVxdWVzdCwgcmVzOiBSZXNwb25zZSkgPT4ge1xyXG4gICAgc2V0SGVhZGVycyhyZXMpO1xyXG5cclxuICAgIGlmICghcmVuZGVyZXIpIHtcclxuICAgICAgcmV0dXJuIHJlcy5lbmQoJ3dhaXRpbmcgZm9yIGNvbXBpbGF0aW9uLi4uIHJlZnJlc2ggaW4gYSBtb21lbnQuJyk7XHJcbiAgICB9XHJcblxyXG4gICAgYWNjZXB0TGFuZ3VhZ2UubGFuZ3VhZ2VzKHBhY2thZ2VKc29uLmNvbmZpZ1snc3VwcG9ydGVkLWxvY2FsZXMnXSk7XHJcblxyXG4gICAgY29uc3Qgc3RhcnRUaW1lOiBudW1iZXIgPSBEYXRlLm5vdygpO1xyXG4gICAgY29uc3QgYWNjZXB0TGFuZzogc3RyaW5nID0gcmVxLmhlYWRlcnNbJ2FjY2VwdC1sYW5ndWFnZSddXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA/IHJlcS5oZWFkZXJzWydhY2NlcHQtbGFuZ3VhZ2UnXS50b1N0cmluZygpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA6IHBhY2thZ2VKc29uLmNvbmZpZ1snZGVmYXVsdC1sb2NhbGUnXTtcclxuICAgIGNvbnN0IGRlZmF1bHRMYW5nOiBzdHJpbmcgPSBhY2NlcHRMYW5ndWFnZS5nZXQoYWNjZXB0TGFuZyk7XHJcbiAgICBjb25zdCBlcnJvckhhbmRsZXIgPSAoZXJyOiBhbnksIHJlbmRlclJlZGlyZWN0OiBhbnkpID0+IHtcclxuICAgICAgaWYgKGVyciAmJiBlcnIuY29kZSA9PT0gNDA0KSB7XHJcbiAgICAgICAgcmVzLnN0YXR1cyg0MDQpO1xyXG4gICAgICAgIExvZ2dlci53YXJuKCd1bnN1cHBvcnRlZCByb3V0ZTogJXM7IGVycm9yOiAlcycsIHJlcS51cmwsIEpTT04uc3RyaW5naWZ5KGVyciwgT2JqZWN0LmdldE93blByb3BlcnR5TmFtZXMoZXJyKSkpO1xyXG4gICAgICAgIHJlbmRlclJlZGlyZWN0KCcvbm90LWZvdW5kJywgdHJ1ZSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgcmVzLnN0YXR1cyg1MDApO1xyXG4gICAgICAgIExvZ2dlci5lcnJvcignZXJyb3IgZHVyaW5nIHJlbmRlcmluZzogJXM7IGVycm9yOiAlcycsIHJlcS51cmwsIEpTT04uc3RyaW5naWZ5KGVyciwgT2JqZWN0LmdldE93blByb3BlcnR5TmFtZXMoZXJyKSkpO1xyXG4gICAgICAgIHJlbmRlclJlZGlyZWN0KCcvZXJyb3InLCB0cnVlKTtcclxuICAgICAgfVxyXG4gICAgfTtcclxuICAgIGNvbnN0IHJlbmRlciA9ICh1cmw6IHN0cmluZywgcmVkaXJlY3Q6IGJvb2xlYW4gPSBmYWxzZSk6IHZvaWQgPT4ge1xyXG4gICAgICBjb25zdCBzZXJ2ZXJDb250ZXh0OiBJU2VydmVyQ29udGV4dCA9IHtcclxuICAgICAgICB1cmwsXHJcbiAgICAgICAgY29va2llczogICAgICAgIHJlcS5jb29raWVzLFxyXG4gICAgICAgIGFjY2VwdExhbmd1YWdlOiBkZWZhdWx0TGFuZyxcclxuICAgICAgICBodG1sTGFuZzogICAgICAgZGVmYXVsdExhbmcuc3Vic3RyKDAsIDIpLFxyXG4gICAgICAgIGFwcENvbmZpZzogICAgICBBcHBDb25maWcsXHJcbiAgICAgICAgcmVkaXJlY3QsXHJcbiAgICAgIH07XHJcblxyXG4gICAgICByZW5kZXJlclxyXG4gICAgICAucmVuZGVyVG9TdHJlYW0oc2VydmVyQ29udGV4dClcclxuICAgICAgLm9uKCdlcnJvcicsIChlcnI6IGFueSkgPT4gZXJyb3JIYW5kbGVyKGVyciwgcmVuZGVyKSlcclxuICAgICAgLm9uKCdlbmQnLCAoKSA9PiBMb2dnZXIuZGVidWcoYHdob2xlIHJlcXVlc3Q6ICR7RGF0ZS5ub3coKSAtIHN0YXJ0VGltZX1tc2ApKVxyXG4gICAgICAucGlwZShyZXMpO1xyXG4gICAgfTtcclxuXHJcbiAgICByZW5kZXIocmVxLnVybCk7XHJcbiAgfSk7XHJcbn07XHJcbiJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/server/routes/SSRRoutes.ts\n");

/***/ }),

/***/ "./src/server/routes/StaticRoutes.ts":
/*!*******************************************!*\
  !*** ./src/server/routes/StaticRoutes.ts ***!
  \*******************************************/
/*! exports provided: StaticRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"StaticRoutes\", function() { return StaticRoutes; });\n/* harmony import */ var serve_favicon__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! serve-favicon */ \"serve-favicon\");\n/* harmony import */ var serve_favicon__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(serve_favicon__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! path */ \"path\");\n/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(path__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _utils_Utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../utils/Utils */ \"./src/server/utils/Utils.ts\");\n/* harmony import */ var _utils_Logger__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../utils/Logger */ \"./src/server/utils/Logger.ts\");\n\r\n\r\n\r\n\r\nvar StaticRoutes = function (app) {\r\n    app.use('/sw.js', Object(_utils_Utils__WEBPACK_IMPORTED_MODULE_2__[\"serve\"])('../client/sw.js'));\r\n    app.use('/', Object(_utils_Utils__WEBPACK_IMPORTED_MODULE_2__[\"serve\"])('../static'));\r\n    app.use('/i18n', Object(_utils_Utils__WEBPACK_IMPORTED_MODULE_2__[\"serve\"])('../../i18n'));\r\n    app.use('/client', Object(_utils_Utils__WEBPACK_IMPORTED_MODULE_2__[\"serve\"])('../client'));\r\n    app.use(serve_favicon__WEBPACK_IMPORTED_MODULE_0__(path__WEBPACK_IMPORTED_MODULE_1__[\"resolve\"](__dirname, '../static/logo.png')));\r\n    app.post('/log/error', function (req, res) {\r\n        var err = req.body.error;\r\n        _utils_Logger__WEBPACK_IMPORTED_MODULE_3__[\"Logger\"].error('error during rendering: %s; error: %s', req.url, JSON.stringify(err, Object.getOwnPropertyNames(err)));\r\n        res.status(200).json({});\r\n    });\r\n};\r\n\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvc2VydmVyL3JvdXRlcy9TdGF0aWNSb3V0ZXMudHM/MjljOCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUVrRDtBQUNUO0FBQ1U7QUFDQztBQUU3QyxJQUFNLFlBQVksR0FBRyxVQUFDLEdBQXdCO0lBQ25ELEdBQUcsQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLDBEQUFLLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDO0lBQzVDLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLDBEQUFLLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztJQUNqQyxHQUFHLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSwwREFBSyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7SUFDdEMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsMERBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO0lBQ3ZDLEdBQUcsQ0FBQyxHQUFHLENBQUMsMENBQU8sQ0FBQyw0Q0FBWSxDQUFDLFNBQVMsRUFBRSxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNoRSxHQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxVQUFDLEdBQVksRUFBRSxHQUFhO1FBQ2pELElBQU0sR0FBRyxHQUFRLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBRWhDLG9EQUFNLENBQUMsS0FBSyxDQUFDLHVDQUF1QyxFQUFFLEdBQUcsQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLEVBQUUsTUFBTSxDQUFDLG1CQUFtQixDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUVySCxHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUMzQixDQUFDLENBQUMsQ0FBQztBQUNMLENBQUMsQ0FBQyIsImZpbGUiOiIuL3NyYy9zZXJ2ZXIvcm91dGVzL1N0YXRpY1JvdXRlcy50cy5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIEV4cHJlc3MgICAgICAgICAgZnJvbSAnZXhwcmVzcyc7XHJcbmltcG9ydCB7IFJlcXVlc3QsIFJlc3BvbnNlIH0gZnJvbSAnZXhwcmVzcyc7XHJcbmltcG9ydCAqIGFzIGZhdmljb24gICAgICAgICAgZnJvbSAnc2VydmUtZmF2aWNvbic7XHJcbmltcG9ydCAqIGFzIHBhdGggICAgICAgICAgICAgZnJvbSAncGF0aCc7XHJcbmltcG9ydCB7IHNlcnZlIH0gICAgICAgICAgICAgZnJvbSAnLi4vdXRpbHMvVXRpbHMnO1xyXG5pbXBvcnQgeyBMb2dnZXIgfSAgICAgICAgICAgIGZyb20gJy4uL3V0aWxzL0xvZ2dlcic7XHJcblxyXG5leHBvcnQgY29uc3QgU3RhdGljUm91dGVzID0gKGFwcDogRXhwcmVzcy5BcHBsaWNhdGlvbikgPT4ge1xyXG4gIGFwcC51c2UoJy9zdy5qcycsIHNlcnZlKCcuLi9jbGllbnQvc3cuanMnKSk7XHJcbiAgYXBwLnVzZSgnLycsIHNlcnZlKCcuLi9zdGF0aWMnKSk7XHJcbiAgYXBwLnVzZSgnL2kxOG4nLCBzZXJ2ZSgnLi4vLi4vaTE4bicpKTtcclxuICBhcHAudXNlKCcvY2xpZW50Jywgc2VydmUoJy4uL2NsaWVudCcpKTtcclxuICBhcHAudXNlKGZhdmljb24ocGF0aC5yZXNvbHZlKF9fZGlybmFtZSwgJy4uL3N0YXRpYy9sb2dvLnBuZycpKSk7XHJcbiAgYXBwLnBvc3QoJy9sb2cvZXJyb3InLCAocmVxOiBSZXF1ZXN0LCByZXM6IFJlc3BvbnNlKSA9PiB7XHJcbiAgICBjb25zdCBlcnI6IGFueSA9IHJlcS5ib2R5LmVycm9yO1xyXG5cclxuICAgIExvZ2dlci5lcnJvcignZXJyb3IgZHVyaW5nIHJlbmRlcmluZzogJXM7IGVycm9yOiAlcycsIHJlcS51cmwsIEpTT04uc3RyaW5naWZ5KGVyciwgT2JqZWN0LmdldE93blByb3BlcnR5TmFtZXMoZXJyKSkpO1xyXG5cclxuICAgIHJlcy5zdGF0dXMoMjAwKS5qc29uKHt9KTtcclxuICB9KTtcclxufTtcclxuIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/server/routes/StaticRoutes.ts\n");

/***/ }),

/***/ "./src/server/server.ts":
/*!******************************!*\
  !*** ./src/server/server.ts ***!
  \******************************/
/*! exports provided: app */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"app\", function() { return app; });\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ \"express\");\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _middlewares__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./middlewares */ \"./src/server/middlewares/index.ts\");\n/* harmony import */ var _routes_DemoRoutes__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./routes/DemoRoutes */ \"./src/server/routes/DemoRoutes.ts\");\n/* harmony import */ var _routes_CounterRoutes__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./routes/CounterRoutes */ \"./src/server/routes/CounterRoutes.ts\");\n/* harmony import */ var _routes_StaticRoutes__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./routes/StaticRoutes */ \"./src/server/routes/StaticRoutes.ts\");\n/* harmony import */ var _routes_SSRRoutes__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./routes/SSRRoutes */ \"./src/server/routes/SSRRoutes.ts\");\n\r\n\r\n\r\n\r\n\r\n\r\nvar app = express__WEBPACK_IMPORTED_MODULE_0__();\r\napp.disable('x-powered-by');\r\nObject(_middlewares__WEBPACK_IMPORTED_MODULE_1__[\"applyMiddlewares\"])(app);\r\n/**\r\n * routes to demonstrate the possibilities of the vue-starter\r\n * can be removed if you don't need them in your application\r\n */\r\nObject(_routes_DemoRoutes__WEBPACK_IMPORTED_MODULE_2__[\"DemoRoutes\"])(app);\r\nObject(_routes_CounterRoutes__WEBPACK_IMPORTED_MODULE_3__[\"CounterRoutes\"])(app);\r\n/**\r\n * core routes, don't delete these\r\n */\r\nObject(_routes_StaticRoutes__WEBPACK_IMPORTED_MODULE_4__[\"StaticRoutes\"])(app);\r\nObject(_routes_SSRRoutes__WEBPACK_IMPORTED_MODULE_5__[\"SSRRoutes\"])(app);\r\n\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvc2VydmVyL3NlcnZlci50cz8zMTc1Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUEyQztBQUNNO0FBQ007QUFDRztBQUNEO0FBQ0g7QUFFL0MsSUFBTSxHQUFHLEdBQXdCLG9DQUFPLEVBQUUsQ0FBQztBQUVsRCxHQUFHLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO0FBRTVCLHFFQUFnQixDQUFDLEdBQUcsQ0FBQyxDQUFDO0FBRXRCOzs7R0FHRztBQUNILHFFQUFVLENBQUMsR0FBRyxDQUFDLENBQUM7QUFDaEIsMkVBQWEsQ0FBQyxHQUFHLENBQUMsQ0FBQztBQUVuQjs7R0FFRztBQUNILHlFQUFZLENBQUMsR0FBRyxDQUFDLENBQUM7QUFDbEIsbUVBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyIsImZpbGUiOiIuL3NyYy9zZXJ2ZXIvc2VydmVyLnRzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0ICogYXMgRXhwcmVzcyAgICAgICAgIGZyb20gJ2V4cHJlc3MnO1xyXG5pbXBvcnQgeyBhcHBseU1pZGRsZXdhcmVzIH0gZnJvbSAnLi9taWRkbGV3YXJlcyc7XHJcbmltcG9ydCB7IERlbW9Sb3V0ZXMgfSAgICAgICBmcm9tICcuL3JvdXRlcy9EZW1vUm91dGVzJztcclxuaW1wb3J0IHsgQ291bnRlclJvdXRlcyB9ICAgIGZyb20gJy4vcm91dGVzL0NvdW50ZXJSb3V0ZXMnO1xyXG5pbXBvcnQgeyBTdGF0aWNSb3V0ZXMgfSAgICAgZnJvbSAnLi9yb3V0ZXMvU3RhdGljUm91dGVzJztcclxuaW1wb3J0IHsgU1NSUm91dGVzIH0gICAgICAgIGZyb20gJy4vcm91dGVzL1NTUlJvdXRlcyc7XHJcblxyXG5leHBvcnQgY29uc3QgYXBwOiBFeHByZXNzLkFwcGxpY2F0aW9uID0gRXhwcmVzcygpO1xyXG5cclxuYXBwLmRpc2FibGUoJ3gtcG93ZXJlZC1ieScpO1xyXG5cclxuYXBwbHlNaWRkbGV3YXJlcyhhcHApO1xyXG5cclxuLyoqXHJcbiAqIHJvdXRlcyB0byBkZW1vbnN0cmF0ZSB0aGUgcG9zc2liaWxpdGllcyBvZiB0aGUgdnVlLXN0YXJ0ZXJcclxuICogY2FuIGJlIHJlbW92ZWQgaWYgeW91IGRvbid0IG5lZWQgdGhlbSBpbiB5b3VyIGFwcGxpY2F0aW9uXHJcbiAqL1xyXG5EZW1vUm91dGVzKGFwcCk7XHJcbkNvdW50ZXJSb3V0ZXMoYXBwKTtcclxuXHJcbi8qKlxyXG4gKiBjb3JlIHJvdXRlcywgZG9uJ3QgZGVsZXRlIHRoZXNlXHJcbiAqL1xyXG5TdGF0aWNSb3V0ZXMoYXBwKTtcclxuU1NSUm91dGVzKGFwcCk7XHJcbiJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/server/server.ts\n");

/***/ }),

/***/ "./src/server/utils/Logger.ts":
/*!************************************!*\
  !*** ./src/server/utils/Logger.ts ***!
  \************************************/
/*! exports provided: Logger */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Logger\", function() { return Logger; });\n/* harmony import */ var winston__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! winston */ \"winston\");\n/* harmony import */ var winston__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(winston__WEBPACK_IMPORTED_MODULE_0__);\n\r\nvar Logger = new winston__WEBPACK_IMPORTED_MODULE_0__[\"Logger\"]({\r\n    transports: [\r\n        new winston__WEBPACK_IMPORTED_MODULE_0__[\"transports\"].File({\r\n            name: 'error',\r\n            filename: 'logs/error.log',\r\n            level: 'error',\r\n            maxFiles: 5,\r\n            maxsize: 10485760,\r\n            json: true,\r\n        }),\r\n        new winston__WEBPACK_IMPORTED_MODULE_0__[\"transports\"].File({\r\n            name: 'all',\r\n            filename: 'logs/all.log',\r\n            maxFiles: 5,\r\n            maxsize: 10485760,\r\n            json: true,\r\n        }),\r\n        new winston__WEBPACK_IMPORTED_MODULE_0__[\"transports\"].Console({\r\n            level: 'debug',\r\n            handleExceptions: true,\r\n            json: false,\r\n            colorize: true,\r\n        }),\r\n    ],\r\n    exitOnError: false,\r\n});\r\n\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvc2VydmVyL3V0aWxzL0xvZ2dlci50cz9kZmUxIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBbUM7QUFFNUIsSUFBTSxNQUFNLEdBQ2pCLElBQUksOENBQWMsQ0FBQztJQUNFLFVBQVUsRUFBRztRQUNYLElBQUksa0RBQWtCLENBQUMsSUFBSSxDQUFDO1lBQ0UsSUFBSSxFQUFNLE9BQU87WUFDakIsUUFBUSxFQUFFLGdCQUFnQjtZQUMxQixLQUFLLEVBQUssT0FBTztZQUNqQixRQUFRLEVBQUUsQ0FBQztZQUNYLE9BQU8sRUFBRyxRQUFRO1lBQ2xCLElBQUksRUFBTSxJQUFJO1NBQ2YsQ0FBQztRQUM5QixJQUFJLGtEQUFrQixDQUFDLElBQUksQ0FBQztZQUNFLElBQUksRUFBTSxLQUFLO1lBQ2YsUUFBUSxFQUFFLGNBQWM7WUFDeEIsUUFBUSxFQUFFLENBQUM7WUFDWCxPQUFPLEVBQUcsUUFBUTtZQUNsQixJQUFJLEVBQU0sSUFBSTtTQUNmLENBQUM7UUFDOUIsSUFBSSxrREFBa0IsQ0FBQyxPQUFPLENBQUM7WUFDRSxLQUFLLEVBQWEsT0FBTztZQUN6QixnQkFBZ0IsRUFBRSxJQUFJO1lBQ3RCLElBQUksRUFBYyxLQUFLO1lBQ3ZCLFFBQVEsRUFBVSxJQUFJO1NBQ3ZCLENBQUM7S0FDbEM7SUFDRCxXQUFXLEVBQUUsS0FBSztDQUNuQixDQUFDLENBQUMiLCJmaWxlIjoiLi9zcmMvc2VydmVyL3V0aWxzL0xvZ2dlci50cy5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIHdpbnN0b24gZnJvbSAnd2luc3Rvbic7XHJcblxyXG5leHBvcnQgY29uc3QgTG9nZ2VyOiB3aW5zdG9uLkxvZ2dlckluc3RhbmNlID1cclxuICBuZXcgd2luc3Rvbi5Mb2dnZXIoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgIHRyYW5zcG9ydHM6ICBbXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICBuZXcgd2luc3Rvbi50cmFuc3BvcnRzLkZpbGUoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogICAgICdlcnJvcicsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmaWxlbmFtZTogJ2xvZ3MvZXJyb3IubG9nJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldmVsOiAgICAnZXJyb3InLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWF4RmlsZXM6IDUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXhzaXplOiAgMTA0ODU3NjAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBqc29uOiAgICAgdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgIG5ldyB3aW5zdG9uLnRyYW5zcG9ydHMuRmlsZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiAgICAgJ2FsbCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmaWxlbmFtZTogJ2xvZ3MvYWxsLmxvZycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXhGaWxlczogNSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1heHNpemU6ICAxMDQ4NTc2MCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGpzb246ICAgICB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgbmV3IHdpbnN0b24udHJhbnNwb3J0cy5Db25zb2xlKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldmVsOiAgICAgICAgICAgICdkZWJ1ZycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVFeGNlcHRpb25zOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAganNvbjogICAgICAgICAgICAgZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2xvcml6ZTogICAgICAgICB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgICAgIF0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgZXhpdE9uRXJyb3I6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICB9KTtcclxuIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/server/utils/Logger.ts\n");

/***/ }),

/***/ "./src/server/utils/Utils.ts":
/*!***********************************!*\
  !*** ./src/server/utils/Utils.ts ***!
  \***********************************/
/*! exports provided: isProd, resolve, serve */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"isProd\", function() { return isProd; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"resolve\", function() { return resolve; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"serve\", function() { return serve; });\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ \"express\");\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! path */ \"path\");\n/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(path__WEBPACK_IMPORTED_MODULE_1__);\n\r\n\r\nvar isProd = \"development\" === 'production';\r\nvar resolve = function (file) { return path__WEBPACK_IMPORTED_MODULE_1__[\"resolve\"](__dirname, file); };\r\nvar serve = function (servePath, cache) {\r\n    if (cache === void 0) { cache = true; }\r\n    return express__WEBPACK_IMPORTED_MODULE_0__[\"static\"](resolve(servePath), {\r\n        maxAge: cache && isProd ? '24h' : 0,\r\n        setHeaders: function (res) {\r\n            res.setHeader('X-Content-Type-Options', 'nosniff');\r\n        },\r\n    });\r\n};\r\n\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvc2VydmVyL3V0aWxzL1V0aWxzLnRzP2IwYTkiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBNEM7QUFFSDtBQUVsQyxJQUFNLE1BQU0sR0FBWSxhQUFvQixLQUFLLFlBQVksQ0FBQztBQUU5RCxJQUFNLE9BQU8sR0FBRyxVQUFDLElBQVksSUFBYSxtREFBWSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsRUFBN0IsQ0FBNkIsQ0FBQztBQUV4RSxJQUFNLEtBQUssR0FBRyxVQUFDLFNBQWlCLEVBQUUsS0FBcUI7SUFBckIsb0NBQXFCO0lBQWMscURBQWMsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLEVBQUU7UUFDN0csTUFBTSxFQUFNLEtBQUssSUFBSSxNQUFNLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN2QyxVQUFVLEVBQUUsVUFBQyxHQUFhO1lBQ3hCLEdBQUcsQ0FBQyxTQUFTLENBQUMsd0JBQXdCLEVBQUUsU0FBUyxDQUFDLENBQUM7UUFDckQsQ0FBQztLQUNGLENBQUM7QUFMMEUsQ0FLMUUsQ0FBQyIsImZpbGUiOiIuL3NyYy9zZXJ2ZXIvdXRpbHMvVXRpbHMudHMuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBFeHByZXNzICAgICAgICAgIGZyb20gJ2V4cHJlc3MnO1xyXG5pbXBvcnQgeyBIYW5kbGVyLCBSZXNwb25zZSB9IGZyb20gJ2V4cHJlc3MnO1xyXG5pbXBvcnQgKiBhcyBwYXRoICAgICAgICAgICAgIGZyb20gJ3BhdGgnO1xyXG5cclxuZXhwb3J0IGNvbnN0IGlzUHJvZDogYm9vbGVhbiA9IHByb2Nlc3MuZW52Lk5PREVfRU5WID09PSAncHJvZHVjdGlvbic7XHJcblxyXG5leHBvcnQgY29uc3QgcmVzb2x2ZSA9IChmaWxlOiBzdHJpbmcpOiBzdHJpbmcgPT4gcGF0aC5yZXNvbHZlKF9fZGlybmFtZSwgZmlsZSk7XHJcblxyXG5leHBvcnQgY29uc3Qgc2VydmUgPSAoc2VydmVQYXRoOiBzdHJpbmcsIGNhY2hlOiBib29sZWFuID0gdHJ1ZSk6IEhhbmRsZXIgPT4gRXhwcmVzcy5zdGF0aWMocmVzb2x2ZShzZXJ2ZVBhdGgpLCB7XHJcbiAgbWF4QWdlOiAgICAgY2FjaGUgJiYgaXNQcm9kID8gJzI0aCcgOiAwLFxyXG4gIHNldEhlYWRlcnM6IChyZXM6IFJlc3BvbnNlKSA9PiB7XHJcbiAgICByZXMuc2V0SGVhZGVyKCdYLUNvbnRlbnQtVHlwZS1PcHRpb25zJywgJ25vc25pZmYnKTtcclxuICB9LFxyXG59KTtcclxuIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/server/utils/Utils.ts\n");

/***/ }),

/***/ 0:
/*!******************************************************!*\
  !*** multi ./src/server/index webpack/hot/poll?1000 ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! ./src/server/index */"./src/server/index.ts");
module.exports = __webpack_require__(/*! webpack/hot/poll?1000 */"./node_modules/webpack/hot/poll.js?1000");


/***/ }),

/***/ "accept-language":
/*!**********************************!*\
  !*** external "accept-language" ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"accept-language\");\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJhY2NlcHQtbGFuZ3VhZ2VcIj9iNGNhIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBIiwiZmlsZSI6ImFjY2VwdC1sYW5ndWFnZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcImFjY2VwdC1sYW5ndWFnZVwiKTsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///accept-language\n");

/***/ }),

/***/ "body-parser":
/*!******************************!*\
  !*** external "body-parser" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"body-parser\");\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJib2R5LXBhcnNlclwiPzgxODgiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEiLCJmaWxlIjoiYm9keS1wYXJzZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJib2R5LXBhcnNlclwiKTsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///body-parser\n");

/***/ }),

/***/ "compression":
/*!******************************!*\
  !*** external "compression" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"compression\");\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJjb21wcmVzc2lvblwiP2Y3OTEiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEiLCJmaWxlIjoiY29tcHJlc3Npb24uanMiLCJzb3VyY2VzQ29udGVudCI6WyJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJjb21wcmVzc2lvblwiKTsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///compression\n");

/***/ }),

/***/ "config":
/*!*************************!*\
  !*** external "config" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"config\");\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJjb25maWdcIj82MzQwIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBIiwiZmlsZSI6ImNvbmZpZy5qcyIsInNvdXJjZXNDb250ZW50IjpbIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcImNvbmZpZ1wiKTsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///config\n");

/***/ }),

/***/ "cookie-parser":
/*!********************************!*\
  !*** external "cookie-parser" ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"cookie-parser\");\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJjb29raWUtcGFyc2VyXCI/MjFkYyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSIsImZpbGUiOiJjb29raWUtcGFyc2VyLmpzIiwic291cmNlc0NvbnRlbnQiOlsibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiY29va2llLXBhcnNlclwiKTsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///cookie-parser\n");

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"express\");\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJleHByZXNzXCI/MjJmZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSIsImZpbGUiOiJleHByZXNzLmpzIiwic291cmNlc0NvbnRlbnQiOlsibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiZXhwcmVzc1wiKTsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///express\n");

/***/ }),

/***/ "fs":
/*!*********************!*\
  !*** external "fs" ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"fs\");\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJmc1wiP2E0MGQiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEiLCJmaWxlIjoiZnMuanMiLCJzb3VyY2VzQ29udGVudCI6WyJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJmc1wiKTsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///fs\n");

/***/ }),

/***/ "http":
/*!***********************!*\
  !*** external "http" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"http\");\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJodHRwXCI/OGQxOSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSIsImZpbGUiOiJodHRwLmpzIiwic291cmNlc0NvbnRlbnQiOlsibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiaHR0cFwiKTsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///http\n");

/***/ }),

/***/ "path":
/*!***********************!*\
  !*** external "path" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"path\");\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJwYXRoXCI/NzRiYiJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSIsImZpbGUiOiJwYXRoLmpzIiwic291cmNlc0NvbnRlbnQiOlsibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicGF0aFwiKTsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///path\n");

/***/ }),

/***/ "serve-favicon":
/*!********************************!*\
  !*** external "serve-favicon" ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"serve-favicon\");\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJzZXJ2ZS1mYXZpY29uXCI/MWRkZCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSIsImZpbGUiOiJzZXJ2ZS1mYXZpY29uLmpzIiwic291cmNlc0NvbnRlbnQiOlsibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwic2VydmUtZmF2aWNvblwiKTsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///serve-favicon\n");

/***/ }),

/***/ "winston":
/*!**************************!*\
  !*** external "winston" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"winston\");\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJ3aW5zdG9uXCI/Nzk3YyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSIsImZpbGUiOiJ3aW5zdG9uLmpzIiwic291cmNlc0NvbnRlbnQiOlsibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwid2luc3RvblwiKTsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///winston\n");

/***/ })

/******/ })));