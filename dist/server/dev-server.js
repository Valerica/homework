(function(e, a) { for(var i in a) e[i] = a[i]; }(exports, /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/server/dev/server.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/server/dev/server.ts":
/*!**********************************!*\
  !*** ./src/server/dev/server.ts ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _utils_Logger__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../utils/Logger */ \"./src/server/utils/Logger.ts\");\n\r\nvar path = function(module){return require(module);}('path');\r\nvar webpack = function(module){return require(module);}('webpack');\r\nvar MFS = function(module){return require(module);}('memory-fs');\r\nvar clientConfig = function(module){return require(module);}('../../config/webpack.client.config');\r\nvar isomorphicConfig = function(module){return require(module);}('../../config/webpack.isomorphic.config');\r\nvar initialized = false;\r\nvar devMiddleware;\r\nvar clientCompiler;\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (function (app, callback) {\r\n    /**\r\n     * Code for hot-reloading\r\n     * ----------------------\r\n     * The dev server and the webpack compilers should just be initialized once.\r\n     * But the middlewares have to be applied every time a new app is loaded.\r\n     */\r\n    if (initialized) {\r\n        app.use(devMiddleware);\r\n        app.use(function(module){return require(module);}('webpack-hot-middleware')(clientCompiler));\r\n        return;\r\n    }\r\n    var bundle;\r\n    var template;\r\n    clientConfig.entry = ['webpack-hot-middleware/client', clientConfig.entry.app];\r\n    clientConfig.output.filename = '[name].js';\r\n    clientConfig.plugins.push(new webpack.HotModuleReplacementPlugin());\r\n    clientConfig.mode = 'development';\r\n    clientCompiler = webpack(clientConfig);\r\n    devMiddleware = function(module){return require(module);}('webpack-dev-middleware')(clientCompiler, {\r\n        publicPath: clientConfig.output.publicPath,\r\n        stats: {\r\n            colors: true,\r\n            chunks: false,\r\n        },\r\n    });\r\n    app.use(devMiddleware);\r\n    clientCompiler.plugin('done', function () {\r\n        var fs = devMiddleware.fileSystem;\r\n        var templatePath = path.join(clientConfig.output.path, 'index.html');\r\n        if (fs.existsSync(templatePath)) {\r\n            template = fs.readFileSync(templatePath, 'utf-8');\r\n            if (bundle) {\r\n                callback(bundle, template);\r\n            }\r\n        }\r\n    });\r\n    app.use(function(module){return require(module);}('webpack-hot-middleware')(clientCompiler));\r\n    isomorphicConfig.mode = 'development';\r\n    var serverCompiler = webpack(isomorphicConfig);\r\n    var mfs = new MFS();\r\n    serverCompiler.outputFileSystem = mfs;\r\n    serverCompiler.watch({}, function (err, stats) {\r\n        if (err) {\r\n            throw err;\r\n        }\r\n        stats = stats.toJson();\r\n        stats.errors.forEach(function (e) { return _utils_Logger__WEBPACK_IMPORTED_MODULE_0__[\"Logger\"].debug('%s', JSON.stringify(err, Object.getOwnPropertyNames(e))); });\r\n        stats.warnings.forEach(function (e) { return _utils_Logger__WEBPACK_IMPORTED_MODULE_0__[\"Logger\"].debug('%s', JSON.stringify(err, Object.getOwnPropertyNames(e))); });\r\n        var bundlePath = path.join(isomorphicConfig.output.path, 'vue-ssr-bundle.json');\r\n        bundle = JSON.parse(mfs.readFileSync(bundlePath, 'utf-8'));\r\n        if (template) {\r\n            callback(bundle, template);\r\n        }\r\n    });\r\n    initialized = true;\r\n});\r\n\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvc2VydmVyL2Rldi9zZXJ2ZXIudHM/OWZlYyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUV1RDtBQUV2RCxJQUFNLElBQUksR0FBRyx5Q0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0FBQ2pDLElBQU0sT0FBTyxHQUFHLHlDQUFXLENBQUMsU0FBUyxDQUFDLENBQUM7QUFDdkMsSUFBTSxHQUFHLEdBQUcseUNBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQztBQUNyQyxJQUFNLFlBQVksR0FBRyx5Q0FBVyxDQUFDLG9DQUFvQyxDQUFDLENBQUM7QUFDdkUsSUFBTSxnQkFBZ0IsR0FBRyx5Q0FBVyxDQUFDLHdDQUF3QyxDQUFDLENBQUM7QUFFL0UsSUFBSSxXQUFXLEdBQVksS0FBSyxDQUFDO0FBQ2pDLElBQUksYUFBbUMsQ0FBQztBQUN4QyxJQUFJLGNBQW1CLENBQUM7QUFFeEIsK0RBQWUsVUFBQyxHQUF3QixFQUFFLFFBQWE7SUFDckQ7Ozs7O09BS0c7SUFDSCxJQUFJLFdBQVcsRUFBRTtRQUNmLEdBQUcsQ0FBQyxHQUFHLENBQUMsYUFBb0IsQ0FBQyxDQUFDO1FBQzlCLEdBQUcsQ0FBQyxHQUFHLENBQUMseUNBQVcsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7UUFDL0QsT0FBTztLQUNSO0lBRUQsSUFBSSxNQUFjLENBQUM7SUFDbkIsSUFBSSxRQUFnQixDQUFDO0lBRXJCLFlBQVksQ0FBQyxLQUFLLEdBQUcsQ0FBQywrQkFBK0IsRUFBRSxZQUFZLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQy9FLFlBQVksQ0FBQyxNQUFNLENBQUMsUUFBUSxHQUFHLFdBQVcsQ0FBQztJQUMzQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLE9BQU8sQ0FBQywwQkFBMEIsRUFBRSxDQUFDLENBQUM7SUFDcEUsWUFBWSxDQUFDLElBQUksR0FBRyxhQUFhLENBQUM7SUFFbEMsY0FBYyxHQUFHLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUN2QyxhQUFhLEdBQUcseUNBQVcsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDLGNBQWMsRUFBRTtRQUNwRSxVQUFVLEVBQUUsWUFBWSxDQUFDLE1BQU0sQ0FBQyxVQUFVO1FBQzFDLEtBQUssRUFBTztZQUNWLE1BQU0sRUFBRSxJQUFJO1lBQ1osTUFBTSxFQUFFLEtBQUs7U0FDZDtLQUNGLENBQUMsQ0FBQztJQUVILEdBQUcsQ0FBQyxHQUFHLENBQUMsYUFBb0IsQ0FBQyxDQUFDO0lBRTlCLGNBQWMsQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFO1FBQzVCLElBQU0sRUFBRSxHQUFRLGFBQWEsQ0FBQyxVQUFVLENBQUM7UUFDekMsSUFBTSxZQUFZLEdBQVcsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxZQUFZLENBQUMsQ0FBQztRQUUvRSxJQUFJLEVBQUUsQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLEVBQUU7WUFDL0IsUUFBUSxHQUFHLEVBQUUsQ0FBQyxZQUFZLENBQUMsWUFBWSxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBRWxELElBQUksTUFBTSxFQUFFO2dCQUNWLFFBQVEsQ0FBQyxNQUFNLEVBQUUsUUFBUSxDQUFDLENBQUM7YUFDNUI7U0FDRjtJQUNILENBQUMsQ0FBQyxDQUFDO0lBRUgsR0FBRyxDQUFDLEdBQUcsQ0FBQyx5Q0FBVyxDQUFDLHdCQUF3QixDQUFDLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztJQUUvRCxnQkFBZ0IsQ0FBQyxJQUFJLEdBQUcsYUFBYSxDQUFDO0lBRXRDLElBQU0sY0FBYyxHQUFRLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO0lBQ3RELElBQU0sR0FBRyxHQUFRLElBQUksR0FBRyxFQUFFLENBQUM7SUFFM0IsY0FBYyxDQUFDLGdCQUFnQixHQUFHLEdBQUcsQ0FBQztJQUN0QyxjQUFjLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBRSxVQUFDLEdBQVEsRUFBRSxLQUFVO1FBQzVDLElBQUksR0FBRyxFQUFFO1lBQ1AsTUFBTSxHQUFHLENBQUM7U0FDWDtRQUNELEtBQUssR0FBRyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDdkIsS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsVUFBQyxDQUFNLElBQUssMkRBQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQXRFLENBQXNFLENBQUMsQ0FBQztRQUN6RyxLQUFLLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxVQUFDLENBQU0sSUFBSywyREFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLEVBQUUsTUFBTSxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBdEUsQ0FBc0UsQ0FBQyxDQUFDO1FBRTNHLElBQU0sVUFBVSxHQUFXLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxxQkFBcUIsQ0FBQyxDQUFDO1FBRTFGLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsVUFBVSxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUM7UUFFM0QsSUFBSSxRQUFRLEVBQUU7WUFDWixRQUFRLENBQUMsTUFBTSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1NBQzVCO0lBQ0gsQ0FBQyxDQUFDLENBQUM7SUFFSCxXQUFXLEdBQUcsSUFBSSxDQUFDO0FBQ3JCLENBQUMsRUFBQyIsImZpbGUiOiIuL3NyYy9zZXJ2ZXIvZGV2L3NlcnZlci50cy5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIEV4cHJlc3MgICAgICAgICAgICAgZnJvbSAnZXhwcmVzcyc7XHJcbmltcG9ydCB7IFdlYnBhY2tEZXZNaWRkbGV3YXJlIH0gZnJvbSAnd2VicGFjay1kZXYtbWlkZGxld2FyZSc7XHJcbmltcG9ydCB7IExvZ2dlciB9ICAgICAgICAgICAgICAgZnJvbSAnLi4vdXRpbHMvTG9nZ2VyJztcclxuXHJcbmNvbnN0IHBhdGggPSBub2RlUmVxdWlyZSgncGF0aCcpO1xyXG5jb25zdCB3ZWJwYWNrID0gbm9kZVJlcXVpcmUoJ3dlYnBhY2snKTtcclxuY29uc3QgTUZTID0gbm9kZVJlcXVpcmUoJ21lbW9yeS1mcycpO1xyXG5jb25zdCBjbGllbnRDb25maWcgPSBub2RlUmVxdWlyZSgnLi4vLi4vY29uZmlnL3dlYnBhY2suY2xpZW50LmNvbmZpZycpO1xyXG5jb25zdCBpc29tb3JwaGljQ29uZmlnID0gbm9kZVJlcXVpcmUoJy4uLy4uL2NvbmZpZy93ZWJwYWNrLmlzb21vcnBoaWMuY29uZmlnJyk7XHJcblxyXG5sZXQgaW5pdGlhbGl6ZWQ6IGJvb2xlYW4gPSBmYWxzZTtcclxubGV0IGRldk1pZGRsZXdhcmU6IFdlYnBhY2tEZXZNaWRkbGV3YXJlO1xyXG5sZXQgY2xpZW50Q29tcGlsZXI6IGFueTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IChhcHA6IEV4cHJlc3MuQXBwbGljYXRpb24sIGNhbGxiYWNrOiBhbnkpOiB2b2lkID0+IHtcclxuICAvKipcclxuICAgKiBDb2RlIGZvciBob3QtcmVsb2FkaW5nXHJcbiAgICogLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAqIFRoZSBkZXYgc2VydmVyIGFuZCB0aGUgd2VicGFjayBjb21waWxlcnMgc2hvdWxkIGp1c3QgYmUgaW5pdGlhbGl6ZWQgb25jZS5cclxuICAgKiBCdXQgdGhlIG1pZGRsZXdhcmVzIGhhdmUgdG8gYmUgYXBwbGllZCBldmVyeSB0aW1lIGEgbmV3IGFwcCBpcyBsb2FkZWQuXHJcbiAgICovXHJcbiAgaWYgKGluaXRpYWxpemVkKSB7XHJcbiAgICBhcHAudXNlKGRldk1pZGRsZXdhcmUgYXMgYW55KTtcclxuICAgIGFwcC51c2Uobm9kZVJlcXVpcmUoJ3dlYnBhY2staG90LW1pZGRsZXdhcmUnKShjbGllbnRDb21waWxlcikpO1xyXG4gICAgcmV0dXJuO1xyXG4gIH1cclxuXHJcbiAgbGV0IGJ1bmRsZTogc3RyaW5nO1xyXG4gIGxldCB0ZW1wbGF0ZTogc3RyaW5nO1xyXG5cclxuICBjbGllbnRDb25maWcuZW50cnkgPSBbJ3dlYnBhY2staG90LW1pZGRsZXdhcmUvY2xpZW50JywgY2xpZW50Q29uZmlnLmVudHJ5LmFwcF07XHJcbiAgY2xpZW50Q29uZmlnLm91dHB1dC5maWxlbmFtZSA9ICdbbmFtZV0uanMnO1xyXG4gIGNsaWVudENvbmZpZy5wbHVnaW5zLnB1c2gobmV3IHdlYnBhY2suSG90TW9kdWxlUmVwbGFjZW1lbnRQbHVnaW4oKSk7XHJcbiAgY2xpZW50Q29uZmlnLm1vZGUgPSAnZGV2ZWxvcG1lbnQnO1xyXG5cclxuICBjbGllbnRDb21waWxlciA9IHdlYnBhY2soY2xpZW50Q29uZmlnKTtcclxuICBkZXZNaWRkbGV3YXJlID0gbm9kZVJlcXVpcmUoJ3dlYnBhY2stZGV2LW1pZGRsZXdhcmUnKShjbGllbnRDb21waWxlciwge1xyXG4gICAgcHVibGljUGF0aDogY2xpZW50Q29uZmlnLm91dHB1dC5wdWJsaWNQYXRoLFxyXG4gICAgc3RhdHM6ICAgICAge1xyXG4gICAgICBjb2xvcnM6IHRydWUsXHJcbiAgICAgIGNodW5rczogZmFsc2UsXHJcbiAgICB9LFxyXG4gIH0pO1xyXG5cclxuICBhcHAudXNlKGRldk1pZGRsZXdhcmUgYXMgYW55KTtcclxuXHJcbiAgY2xpZW50Q29tcGlsZXIucGx1Z2luKCdkb25lJywgKCkgPT4ge1xyXG4gICAgY29uc3QgZnM6IGFueSA9IGRldk1pZGRsZXdhcmUuZmlsZVN5c3RlbTtcclxuICAgIGNvbnN0IHRlbXBsYXRlUGF0aDogc3RyaW5nID0gcGF0aC5qb2luKGNsaWVudENvbmZpZy5vdXRwdXQucGF0aCwgJ2luZGV4Lmh0bWwnKTtcclxuXHJcbiAgICBpZiAoZnMuZXhpc3RzU3luYyh0ZW1wbGF0ZVBhdGgpKSB7XHJcbiAgICAgIHRlbXBsYXRlID0gZnMucmVhZEZpbGVTeW5jKHRlbXBsYXRlUGF0aCwgJ3V0Zi04Jyk7XHJcblxyXG4gICAgICBpZiAoYnVuZGxlKSB7XHJcbiAgICAgICAgY2FsbGJhY2soYnVuZGxlLCB0ZW1wbGF0ZSk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9KTtcclxuXHJcbiAgYXBwLnVzZShub2RlUmVxdWlyZSgnd2VicGFjay1ob3QtbWlkZGxld2FyZScpKGNsaWVudENvbXBpbGVyKSk7XHJcblxyXG4gIGlzb21vcnBoaWNDb25maWcubW9kZSA9ICdkZXZlbG9wbWVudCc7XHJcblxyXG4gIGNvbnN0IHNlcnZlckNvbXBpbGVyOiBhbnkgPSB3ZWJwYWNrKGlzb21vcnBoaWNDb25maWcpO1xyXG4gIGNvbnN0IG1mczogYW55ID0gbmV3IE1GUygpO1xyXG5cclxuICBzZXJ2ZXJDb21waWxlci5vdXRwdXRGaWxlU3lzdGVtID0gbWZzO1xyXG4gIHNlcnZlckNvbXBpbGVyLndhdGNoKHt9LCAoZXJyOiBhbnksIHN0YXRzOiBhbnkpID0+IHtcclxuICAgIGlmIChlcnIpIHtcclxuICAgICAgdGhyb3cgZXJyO1xyXG4gICAgfVxyXG4gICAgc3RhdHMgPSBzdGF0cy50b0pzb24oKTtcclxuICAgIHN0YXRzLmVycm9ycy5mb3JFYWNoKChlOiBhbnkpID0+IExvZ2dlci5kZWJ1ZygnJXMnLCBKU09OLnN0cmluZ2lmeShlcnIsIE9iamVjdC5nZXRPd25Qcm9wZXJ0eU5hbWVzKGUpKSkpO1xyXG4gICAgc3RhdHMud2FybmluZ3MuZm9yRWFjaCgoZTogYW55KSA9PiBMb2dnZXIuZGVidWcoJyVzJywgSlNPTi5zdHJpbmdpZnkoZXJyLCBPYmplY3QuZ2V0T3duUHJvcGVydHlOYW1lcyhlKSkpKTtcclxuXHJcbiAgICBjb25zdCBidW5kbGVQYXRoOiBzdHJpbmcgPSBwYXRoLmpvaW4oaXNvbW9ycGhpY0NvbmZpZy5vdXRwdXQucGF0aCwgJ3Z1ZS1zc3ItYnVuZGxlLmpzb24nKTtcclxuXHJcbiAgICBidW5kbGUgPSBKU09OLnBhcnNlKG1mcy5yZWFkRmlsZVN5bmMoYnVuZGxlUGF0aCwgJ3V0Zi04JykpO1xyXG5cclxuICAgIGlmICh0ZW1wbGF0ZSkge1xyXG4gICAgICBjYWxsYmFjayhidW5kbGUsIHRlbXBsYXRlKTtcclxuICAgIH1cclxuICB9KTtcclxuXHJcbiAgaW5pdGlhbGl6ZWQgPSB0cnVlO1xyXG59O1xyXG4iXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./src/server/dev/server.ts\n");

/***/ }),

/***/ "./src/server/utils/Logger.ts":
/*!************************************!*\
  !*** ./src/server/utils/Logger.ts ***!
  \************************************/
/*! exports provided: Logger */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Logger\", function() { return Logger; });\n/* harmony import */ var winston__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! winston */ \"winston\");\n/* harmony import */ var winston__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(winston__WEBPACK_IMPORTED_MODULE_0__);\n\r\nvar Logger = new winston__WEBPACK_IMPORTED_MODULE_0__[\"Logger\"]({\r\n    transports: [\r\n        new winston__WEBPACK_IMPORTED_MODULE_0__[\"transports\"].File({\r\n            name: 'error',\r\n            filename: 'logs/error.log',\r\n            level: 'error',\r\n            maxFiles: 5,\r\n            maxsize: 10485760,\r\n            json: true,\r\n        }),\r\n        new winston__WEBPACK_IMPORTED_MODULE_0__[\"transports\"].File({\r\n            name: 'all',\r\n            filename: 'logs/all.log',\r\n            maxFiles: 5,\r\n            maxsize: 10485760,\r\n            json: true,\r\n        }),\r\n        new winston__WEBPACK_IMPORTED_MODULE_0__[\"transports\"].Console({\r\n            level: 'debug',\r\n            handleExceptions: true,\r\n            json: false,\r\n            colorize: true,\r\n        }),\r\n    ],\r\n    exitOnError: false,\r\n});\r\n\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvc2VydmVyL3V0aWxzL0xvZ2dlci50cz9kZmUxIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBbUM7QUFFNUIsSUFBTSxNQUFNLEdBQ2pCLElBQUksOENBQWMsQ0FBQztJQUNFLFVBQVUsRUFBRztRQUNYLElBQUksa0RBQWtCLENBQUMsSUFBSSxDQUFDO1lBQ0UsSUFBSSxFQUFNLE9BQU87WUFDakIsUUFBUSxFQUFFLGdCQUFnQjtZQUMxQixLQUFLLEVBQUssT0FBTztZQUNqQixRQUFRLEVBQUUsQ0FBQztZQUNYLE9BQU8sRUFBRyxRQUFRO1lBQ2xCLElBQUksRUFBTSxJQUFJO1NBQ2YsQ0FBQztRQUM5QixJQUFJLGtEQUFrQixDQUFDLElBQUksQ0FBQztZQUNFLElBQUksRUFBTSxLQUFLO1lBQ2YsUUFBUSxFQUFFLGNBQWM7WUFDeEIsUUFBUSxFQUFFLENBQUM7WUFDWCxPQUFPLEVBQUcsUUFBUTtZQUNsQixJQUFJLEVBQU0sSUFBSTtTQUNmLENBQUM7UUFDOUIsSUFBSSxrREFBa0IsQ0FBQyxPQUFPLENBQUM7WUFDRSxLQUFLLEVBQWEsT0FBTztZQUN6QixnQkFBZ0IsRUFBRSxJQUFJO1lBQ3RCLElBQUksRUFBYyxLQUFLO1lBQ3ZCLFFBQVEsRUFBVSxJQUFJO1NBQ3ZCLENBQUM7S0FDbEM7SUFDRCxXQUFXLEVBQUUsS0FBSztDQUNuQixDQUFDLENBQUMiLCJmaWxlIjoiLi9zcmMvc2VydmVyL3V0aWxzL0xvZ2dlci50cy5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIHdpbnN0b24gZnJvbSAnd2luc3Rvbic7XHJcblxyXG5leHBvcnQgY29uc3QgTG9nZ2VyOiB3aW5zdG9uLkxvZ2dlckluc3RhbmNlID1cclxuICBuZXcgd2luc3Rvbi5Mb2dnZXIoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgIHRyYW5zcG9ydHM6ICBbXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICBuZXcgd2luc3Rvbi50cmFuc3BvcnRzLkZpbGUoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogICAgICdlcnJvcicsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmaWxlbmFtZTogJ2xvZ3MvZXJyb3IubG9nJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldmVsOiAgICAnZXJyb3InLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbWF4RmlsZXM6IDUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXhzaXplOiAgMTA0ODU3NjAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBqc29uOiAgICAgdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgIG5ldyB3aW5zdG9uLnRyYW5zcG9ydHMuRmlsZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiAgICAgJ2FsbCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmaWxlbmFtZTogJ2xvZ3MvYWxsLmxvZycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXhGaWxlczogNSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1heHNpemU6ICAxMDQ4NTc2MCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGpzb246ICAgICB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgbmV3IHdpbnN0b24udHJhbnNwb3J0cy5Db25zb2xlKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldmVsOiAgICAgICAgICAgICdkZWJ1ZycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVFeGNlcHRpb25zOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAganNvbjogICAgICAgICAgICAgZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2xvcml6ZTogICAgICAgICB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgICAgIF0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgZXhpdE9uRXJyb3I6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICB9KTtcclxuIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/server/utils/Logger.ts\n");

/***/ }),

/***/ "winston":
/*!**************************!*\
  !*** external "winston" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"winston\");\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJ3aW5zdG9uXCI/Nzk3YyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSIsImZpbGUiOiJ3aW5zdG9uLmpzIiwic291cmNlc0NvbnRlbnQiOlsibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwid2luc3RvblwiKTsiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///winston\n");

/***/ })

/******/ })));